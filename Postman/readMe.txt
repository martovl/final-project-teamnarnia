Prerequisits for running project:
1.Heidi
2.Java 11
3.InteliJ Idea

Instructions for starting the project:

1.Download the project from https://gitlab.com/martovl/final-project-teamnarnia
2.Open Heidi and load Final_Project_Narnia_Postman_Database.sql file from directory - final-project-teamnarnia\Postman\database in order to generate database for project. Tests should run on newly loaded database!

Prerequisits for running Postman tests:
1.Postman
2.Gradle
3.Newman, npm install -g newman
4.Newman reporter, npm install -g newman reporter-htmlextra
5.Download project with postman with folder "Postman" which contains Postman files.

Instructions for running the project:

1.Postman tests are running on localhost:8080, so in order to start you should make changes in Final project Narnia.postman_environment file.
1.1.You need to run project locally.
1.2.Navigate to folder \final-project-teamnarnia DevProject\socialnetwork\project
1.3.In cmd type gradle and wait for message "BUILD SUCCESSFULL", after that type gradle bootRun and wait to get "75% EXECUTING"
1.4.Open localhost:8080 in your browser
2.Open Final project Narnia.postman_environment with text editor (e.g Notepad,Notepad++).
2.1.You need to get 3 cookies after opening localhost:8080 -> Developer tools -> Application.The copy the value of JSESSIONID in the following field:
2.2. You need to change the value of 		"key": "user7_cookie",
						"value": "JSESSIONID=Type your cookie here" 

						"key": "user8_cookie",
						"value": "JSESSIONID=Type your cookie here" 


						"key": "admin_cookie",
						"value": "JSESSIONID=Type your cookie here" 

2.3.You need to sign up with different credentials,after that open new browser and sign up with different user: 

						User7 username - 10@gmail.com
						Password - Narniaqa22@
								
						User7 username - 12@gmail.com
						Password - Narniaqa22@
						
						Admin username - finalprojectnarnia@gmail.com
						Password - Narniaqa22@

												
3.After you have changed your cookies, save the file with the same name so you can replace it.
4.Navigate to \final-project-teamnarnia Postman and run the Narnia.bat
5.After tests are executed, navigate to final-project-teamnarnia\Postman\newman and open the generated html report.

