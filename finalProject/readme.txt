Allure Installation


To install Allure you will need scoope

1.Install scoop - more information at: https://scoop.sh/
1.1 Write in PowerShell this command - iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
1.2 If you get an error you might need to change the execution policy (i.e. enable Powershell) with
this command - Set-ExecutionPolicy RemoteSigned -scope CurrentUser
2.Check if you have scoop is ready by typing scoop in cmd and check if it return scoop commands
3.Type in cmd scoop install allure
4.Navigate allure bin directory to system varibles
5.Add allure to system variables
6.Check if allure --version returns result about allure version you have. 


Maven

1.Java 11,Make sure JDK is installed and JAVA_HOME enviroment variable is configured.
2.Download Maven http://maven.apache.org/download.cgi
2.1 Additional info for Maven https://howtodoinjava.com/maven/how-to-install-maven-on-windows/
3.Check if Maven is download:mvn --version

Starting QA project Healthy food social-network 

Prerequisites:

1.Heidi
2.Java 11
3.Intellij Idea


Instructions for starting the project:

1.Download the project from https://gitlab.com/martovl/final-project-teamnarnia
2.Open Heidi and load Final_Project_Narnia_Selenium_Database.sql file from directory - final-project-teamnarnia\finalProject\database directory in order to generate database for the project. Test should run on newly loaded database.


Instructions for running the tests:

1.Test are configured to run on Chrome latest version.If you want to run them on Firefox,please remove "\\" before "chooseBrowser=Firefox and write "\\"before "chooseBrowser=Chrome in \scr\test\resources\config
2.Run test suites and BBD from TeamNarniaRunTestsSuiteScript and TeamNarniaRunSciptsBBD. Please have in mind scripts are downloading the whole repository with SSH key,so you must have already created SSH key.
3.Allure report will be visible on the screen.
