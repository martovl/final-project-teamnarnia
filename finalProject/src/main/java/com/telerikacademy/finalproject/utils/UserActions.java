package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {


    final WebDriver driver;
    final WebDriverWait wait;

    public UserActions() {
        this.driver = Utils.getWebDriver();
        this.wait = new WebDriverWait(driver, 30);
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(key))));

        element.click();
    }

    public void clearAndTypeValueInField(String value, String field, Object... fieldArguments) {
        Utils.LOG.info("Clearing and typing " + value + " in " + field);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(field))));
        element.clear();
        element.sendKeys(value);
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }


    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public void scrollToElement(String key) {
        Utils.LOG.info("Scrolling to element " + key);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void mouseHoverElement(String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Utils.LOG.info("Hovering on element " + key);
        Actions a = new Actions(driver);
        a.moveToElement(element).perform();
    }


    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

}

