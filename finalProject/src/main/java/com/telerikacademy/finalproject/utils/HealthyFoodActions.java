package com.telerikacademy.finalproject.utils;


import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import static com.telerikacademy.finalproject.utils.Constants.*;

public class HealthyFoodActions extends UserActions {

    //############# ELEMENT OPERATIONS #########

    //Creates random String
    public String createRandomString(int n) {
        Utils.LOG.info("Creating random string");
        return RandomStringUtils.randomAlphabetic(n);
    }

    //TC028_memberSuccessfulSendsRequest
    public void sentUserConnectionRequest() {
        Utils.LOG.info("User send connection request");

        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        mouseHoverElement("USERS.SectionUsers");
        clickElement("USERS.SectionUsersDropDownMenu");
        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        clickElement("USERS.SectionUsers");
        isElementPresentUntilTimeout("USERS.ListUsers", 30);
        isElementPresentUntilTimeout("USERS.SearchedUser", 30);
        clickElement("USERS.SearchedUser");
        isElementPresentUntilTimeout("USERS.ConnectButton", 30);
        clickElement("USERS.ConnectButton");
        isElementPresentUntilTimeout("USERS.Sent(Reject)RequestButton", 30);
    }

    //TC029_memberSuccessfulAcceptRequest
    public void acceptUserConnectionRequest() {
        Utils.LOG.info("User accepts friend request");

        isElementPresentUntilTimeout("navigation.UsersMenu", 40);
        mouseHoverElement("navigation.UsersMenu");
        isElementPresentUntilTimeout("USERS.RequestYellowBellDropDown", 40);
        clickElement("USERS.RequestYellowBellDropDown");
        clickElement("USERS.RequestSender");
        isElementPresentUntilTimeout("USERS.ConfirmButton", 40);
        clickElement("USERS.ConfirmButton");
        isElementPresentUntilTimeout("USERS.DisconnectButton", 40);
    }

    //TC030_memberSuccessfulDisconnectsFriend
    public void userDisconnectsFriend() {
        Utils.LOG.info("User disconnects friend");

        assertElementPresent("navigation.UsersMenu");
        mouseHoverElement("navigation.UsersMenu");
        clickElement("USERS.FriendsDropDownItem");
        isElementPresentUntilTimeout("USERS.ListFriends", 30);
        clickElement("USERS.SearchedFriendDisconnecting");
        isElementPresentUntilTimeout("USERS.DisconnectButton",30);
        clickElement("USERS.DisconnectButton");
        isElementPresentUntilTimeout("USERS.ConnectButton", 30);
    }

    // TC040_memberSuccessfulCreatesConnectionsPostWithDescription
    public void selectByValue(String key, String value) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Select selectObject = new Select(element);
        selectObject.selectByValue(value);
    }

    //TC040_memberSuccessfulCreatesConnectionsPostWithDescription
    public void userCreatesConnectionsPost() throws URISyntaxException {
        Utils.LOG.info("User creates connection post");

        isElementPresentUntilTimeout("navigation.LatestPosts", 40);
        mouseHoverElement("navigation.LatestPosts");
        assertElementPresent("POSTS.NewPostDropDownItem");
        clickElement("POSTS.NewPostDropDownItem");
        isElementPresentUntilTimeout("POSTS.PostPicture", 40);
        selectByValue("POSTS.PostVisibility", "2");
        isElementPresentUntilTimeout("POSTS.Title", 40);
        clearAndTypeValueInField(CONNECTIONS_TITLE_POST, "POSTS.Title");
        isElementPresentUntilTimeout("POSTS.Description", 40);
        clearAndTypeValueInField(CONNECTIONS_POST_DESCRIPTION, "POSTS.Description");
        isElementPresentUntilTimeout("POSTS.ChooseFilePostButton", 40);
        URL url = getClass().getResource("/UploadFiles/milk.jpg");
        File file = Paths.get(url.toURI()).toFile();
        clearAndTypeValueInField(file.getAbsolutePath(), "POSTS.ChooseFilePostButton");
        clickElement("POSTS.SaveButton");
    }

    //TC043_memberSuccessfulUpdatesFirstNameANDLastNameAndAdditionalInformationForItself
    public void updateUsersInfo() {
        Utils.LOG.info("User updates information about itself");

        isElementPresentUntilTimeout("navigation.Profile", 30);
        mouseHoverElement("navigation.Profile");
        isElementPresentUntilTimeout("PROFILE.EditDropDownItem", 30);
        clickElement("PROFILE.EditDropDownItem");

        isElementPresentUntilTimeout("PROFILE.FirstNameField", 30);
        clearAndTypeValueInField(Constants.UPDATED_FIRST_NAME, "PROFILE.FirstNameField");

        isElementPresentUntilTimeout("PROFILE.LastNameField", 30);
        clearAndTypeValueInField(Constants.UPDATED_LAST_NAME, "PROFILE.LastNameField");

        isElementPresentUntilTimeout("PROFILE.NationalityField", 30);
        typeValueInField(Constants.UPDATED_NATIONALITY, "PROFILE.NationalityField");

        isElementPresentUntilTimeout("PROFILE.GenderField", 30);
        typeValueInField(Constants.UPDATED_GENDER, "PROFILE.GenderField");

        isElementPresentUntilTimeout("PROFILE.AgeField", 30);
        clearAndTypeValueInField(Constants.UPDATED_AGE, "PROFILE.AgeField");

        isElementPresentUntilTimeout("PROFILE.AdditionalInfoField", 30);
        clearAndTypeValueInField(Constants.UPDATED_ADDITIONAL_INFO, "PROFILE.AdditionalInfoField");

        clickElement("PROFILE.SaveChangesButton");
    }

    //TC049_memberSuccessfulChangesProfilePictureJPEGFormat
    public void updateUserProfilePicture() throws URISyntaxException {
        Utils.LOG.info("User updates his profile picture");

        isElementPresentUntilTimeout("navigation.Profile", 30);
        mouseHoverElement("navigation.Profile");
        isElementPresentUntilTimeout("PROFILE.EditDropDownItem", 30);
        clickElement("PROFILE.EditDropDownItem");
        isElementPresentUntilTimeout("PROFILE.ChooseFileUserButton", 30);
        URL url = getClass().getResource("/UploadFiles/2.jpg");
        File file = Paths.get(url.toURI()).toFile();
        isElementPresentUntilTimeout("PROFILE.ChooseFileUserButton", 30);
        clearAndTypeValueInField(file.getAbsolutePath(), "PROFILE.ChooseFileUserButton");
        clickElement("PROFILE.SaveChangesButton");
    }

    //TC_063 AdminSuccessfulCreatesNewCategory
    public void createAdminCategory() throws URISyntaxException {
        Utils.LOG.info("Admin create category");

        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        clickElement("ADMIN.Categories");
        clickElement("ADMIN.AddButton");
        isElementPresentUntilTimeout("ADMIN.ChooseFileCategoryButton", 40);
        URL url = getClass().getResource("/UploadFiles/drinks.png");
        File file = Paths.get(url.toURI()).toFile();
        clearAndTypeValueInField(file.getAbsolutePath(), "ADMIN.ChooseFileCategoryButton");
        isElementPresentUntilTimeout("ADMIN.CategoryName", 40);
        clearAndTypeValueInField(CATEGORY_NAME, "ADMIN.CategoryName");
        clickElement("ADMIN.SaveButton");
        isElementPresentUntilTimeout("ADMIN.AddButton", 40);
    }

    //TC_064 AdminSuccessfulDeletesCategory
    public void deleteAdminCategory() {
        Utils.LOG.info("Admin delete category");

        isElementPresentUntilTimeout("ADMIN.EditDesiredCategory", 30);
        clickElement("ADMIN.EditButton");
        isElementPresentUntilTimeout("ADMIN.DeleteButton", 30);
        clickElement("ADMIN.DeleteButton");
        isElementPresentUntilTimeout("ADMIN.YesButton", 30);
        clickElement("ADMIN.YesButton");
        isElementPresentUntilTimeout("ADMIN.AddButton", 30);
    }

    //TC_065 AdminSuccessfulDeletesPost
    public void deleteAdminConnectionsPost() {
        Utils.LOG.info("Admin delete connection post");

        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        clickElement("ADMIN.Posts");
        isElementPresentUntilTimeout("ADMIN.ListPosts", 40);
        clearAndTypeValueInField(CONNECTIONS_TITLE_POST, "ADMIN.SearchKeywordInput");
        clickElement("ADMIN.SearchButton");
        isElementPresentUntilTimeout("ADMIN.SearchedPostForDeleting", 40);
        clickElement("ADMIN.SearchedPostForDeleting");
        isElementPresentUntilTimeout("ADMIN.EditItem", 40);
        clickElement("ADMIN.EditItem");
        isElementPresentUntilTimeout("ADMIN.DeleteButton", 40);
        clickElement("ADMIN.DeleteButton");
        isElementPresentUntilTimeout("ADMIN.YesButton", 40);
        clickElement("ADMIN.YesButton");
        isElementPresentUntilTimeout("ADMIN.FirstName", 40);
    }

    //TC073_successfulRegistrationWithPasswordLength8SymbolsContainingAllMandatoryCharacters
    public void registerUser() throws URISyntaxException {
        Utils.LOG.info("Make successful registration");

        clickElement("loginPage.RegisterButton");
        isElementPresentUntilTimeout("REGISTRATION.Header", 40);

        isElementPresentUntilTimeout("REGISTRATION.EmailInput", 40);
        clearAndTypeValueInField(createRandomString(5) + REGISTRATION_USERNAME, "REGISTRATION.EmailInput");

        isElementPresentUntilTimeout("REGISTRATION.Age", 40);
        clearAndTypeValueInField(REGISTRATION_AGE, "REGISTRATION.Age");

        isElementPresentUntilTimeout("REGISTRATION.PasswordInput", 40);
        clearAndTypeValueInField(PASSWORD, "REGISTRATION.PasswordInput");

        isElementPresentUntilTimeout("REGISTRATION.PasswordConfirmationInput", 40);
        clearAndTypeValueInField(PASSWORD, "REGISTRATION.PasswordConfirmationInput");

        isElementPresentUntilTimeout("REGISTRATION.FirstNameInput", 40);
        typeValueInField(REGISTRATION_FIRST_NAME, "REGISTRATION.FirstNameInput");

        isElementPresentUntilTimeout("REGISTRATION.LastNameInput", 40);
        typeValueInField(REGISTRATION_LAST_NAME, "REGISTRATION.LastNameInput");

        isElementPresentUntilTimeout("REGISTRATION.Nationality", 40);
        typeValueInField(REGISTRATION_NATIONALITY, "REGISTRATION.Nationality");

        isElementPresentUntilTimeout("REGISTRATION.Gender", 40);
        typeValueInField(REGISTRATION_GENDER, "REGISTRATION.Gender");

        isElementPresentUntilTimeout("REGISTRATION.AboutInput", 40);
        typeValueInField(REGISTRATION_ADDITIONAL_INFO, "REGISTRATION.AboutInput");

        isElementPresentUntilTimeout("REGISTRATION.Visibility", 40);
        typeValueInField(REGISTRATION_PROFILE_PICTURE_VISIBILITY, "REGISTRATION.Visibility");

        URL url = getClass().getResource("/UploadFiles/drinks.png");
        File file = Paths.get(url.toURI()).toFile();
        typeValueInField(file.getAbsolutePath(), "REGISTRATION.ChooseFileProfilePicture");

        isElementPresentUntilTimeout("REGISTRATION.RegisterButton", 40);
        clickElement("REGISTRATION.RegisterButton");
    }

    //TC_054_memberSuccessfulLikesFriendPost
    public void userLikesFriendPost() {
        Utils.LOG.info("User likes friend post");

        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Like.RecentPost", 40);
        isElementPresentUntilTimeout("Like.SearchedPost", 40);
        clickElement("Like.SearchedPost");
        isElementPresentUntilTimeout("Like.SearchedPostHeader", 40);
        clickElement("Friends.LikeButton");
    }

    //TC_056_memberSuccessfulDislikesFriendPost
    public void userDislikesFriendPost() {
        Utils.LOG.info("User dislikes friend post");

        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Dislike.RecentPost", 30);
        isElementPresentUntilTimeout("Dislike.SearchedPost", 30);
        clickElement("Dislike.SearchedPost");
        isElementPresentUntilTimeout("Dislike.SearchedPostHeader", 30);
        clickElement("Friends.DislikeButton");
    }

    //TC058_memberSuccessfulCommentsFriendPost
    public void userCommentFriendPost() {
        Utils.LOG.info("User comments friend post");

        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Dislike.RecentPost", 30);
        isElementPresentUntilTimeout("Dislike.SearchedPost", 30);
        clickElement("Dislike.SearchedPost");
        isElementPresentUntilTimeout("Dislike.SearchedPostHeader", 30);
        isElementPresentUntilTimeout("Comment.Field", 30);
        clearAndTypeValueInField(COMMENT_FRIEND_POST, "Comment.Field");
        clickElement("Comment.SendButton");
    }

    //functional operations
    public void fillRegistrationForm() throws URISyntaxException {

        isElementPresentUntilTimeout("REGISTRATION.EmailInput", 60);
        clearAndTypeValueInField(createRandomString(5) + REGISTRATION_USERNAME, "REGISTRATION.EmailInput");

        isElementPresentUntilTimeout("REGISTRATION.Age", 40);
        clearAndTypeValueInField(REGISTRATION_AGE, "REGISTRATION.Age");

        isElementPresentUntilTimeout("REGISTRATION.PasswordInput", 40);
        clearAndTypeValueInField(PASSWORD, "REGISTRATION.PasswordInput");

        isElementPresentUntilTimeout("REGISTRATION.PasswordConfirmationInput", 40);
        clearAndTypeValueInField(PASSWORD, "REGISTRATION.PasswordConfirmationInput");

        isElementPresentUntilTimeout("REGISTRATION.FirstNameInput", 40);
        typeValueInField(REGISTRATION_FIRST_NAME, "REGISTRATION.FirstNameInput");

        isElementPresentUntilTimeout("REGISTRATION.LastNameInput", 40);
        typeValueInField(REGISTRATION_LAST_NAME, "REGISTRATION.LastNameInput");

        isElementPresentUntilTimeout("REGISTRATION.Nationality", 40);
        typeValueInField(REGISTRATION_NATIONALITY, "REGISTRATION.Nationality");

        isElementPresentUntilTimeout("REGISTRATION.Gender", 40);
        typeValueInField(REGISTRATION_GENDER, "REGISTRATION.Gender");

        isElementPresentUntilTimeout("REGISTRATION.AboutInput", 40);
        typeValueInField(REGISTRATION_ADDITIONAL_INFO, "REGISTRATION.AboutInput");

        isElementPresentUntilTimeout("REGISTRATION.Visibility", 40);
        typeValueInField(REGISTRATION_PROFILE_PICTURE_VISIBILITY, "REGISTRATION.Visibility");

        URL url = getClass().getResource("/UploadFiles/drinks.png");
        File file = Paths.get(url.toURI()).toFile();
        typeValueInField(file.getAbsolutePath(), "REGISTRATION.ChooseFileProfilePicture");

        isElementPresentUntilTimeout("REGISTRATION.RegisterButton", 40);
        clickElement("REGISTRATION.RegisterButton");
    }

    public void disconnectUser2(){
        assertElementPresent("navigation.UsersMenu");
        mouseHoverElement("navigation.UsersMenu");
        clickElement("USERS.FriendsDropDownItem");
        isElementPresentUntilTimeout("USERS.ListFriends", 50);
        clickElement("USERS.SearchedFriendDisconnecting2");
        isElementPresentUntilTimeout("USERS.DisconnectButton",50);
        clickElement("USERS.DisconnectButton");
        isElementPresentUntilTimeout("USERS.ConnectButton", 50);

    }


    //############# ASSERTS #########

    public void assertRequestIsSent() {
        isElementPresentUntilTimeout("USERS.Sent(Reject)RequestButton",50);
        assertElementPresent("USERS.Sent(Reject)RequestButton");
    }


    //TC029_memberSuccessfulAcceptRequest
    public void assertRequestIsAccepted() {
        Utils.LOG.info("Check if users are connected");

        assertElementPresent("navigation.UsersMenu");
        mouseHoverElement("navigation.UsersMenu");
        assertElementPresent("USERS.FriendsDropDownItem");
        clickElement("USERS.FriendsDropDownItem");
        isElementPresentUntilTimeout("USERS.ListFriends", 30);
        assertElementPresent("USERS.SearchedFriend");
    }

    //TC030_memberSuccessfulDisconnectsFriend
    public void assertUsersAreDisconnected() {
        Utils.LOG.info("Check if users are disconnected");

        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        clickElement("USERS.SectionUsers");
        isElementPresentUntilTimeout("USERS.ListUsers", 30);
        isElementPresentUntilTimeout("USERS.SearchedFriendDisconnecting", 30);
        clickElement("USERS.SearchedFriendDisconnecting");
        assertElementPresent("USERS.ConnectButton");
    }

    //TC040_memberSuccessfulCreatesConnectionsPostWithDescription()
    public void assertPostIsCreated() {
        Utils.LOG.info("Check if post is created");

        isElementPresentUntilTimeout("POSTS.ListAllPosts", 30);
        scrollToElement("POSTS.CreatedPost");
        clickElement("POSTS.CreatedPost");
        assertElementPresent("POSTS.DescriptionUnderPost");
    }


    //TC049_memberSuccessfulChangesProfilePictureJPEGFormat()
    public void assertProfilePictureIsChanged() {
        Utils.LOG.info("Check if profile picture is changed");

        assertElementPresent("PROFILE.EditButton");
    }

    //TC_054_memberSuccessfulLikesFriendPost
    public void assertFriendPostIsLiked() {
        Utils.LOG.info("Check if friend post is liked");

        isElementPresentUntilTimeout("Friends.DislikeButton", 30);
        assertElementPresent("Friends.DislikeButton");
    }

    //TC_056_memberSuccessfulDislikesFriendPost
    public void assertFriendPostIsDisliked() {
        Utils.LOG.info("Check if friend post is disliked");

        isElementPresentUntilTimeout("Friends.LikeButton", 30);
        assertElementPresent("Friends.LikeButton");

    }

    //TC058_memberSuccessfulCommentsFriendPost
    public void assertFriendPostIsCommented() {
        Utils.LOG.info("Check if friend post is commented");

        isElementPresentUntilTimeout("Comment.SuccessfulComment", 30);
        assertElementPresent("Comment.SuccessfulComment");
    }

    //TC_063 AdminSuccessfulCreatesNewCategory()
    public void assertCategoryIsCreated() {
        Utils.LOG.info("Check if category is created");

        isElementPresentUntilTimeout("ADMIN.CreatedCategory", 30);
        assertElementPresent("ADMIN.CreatedCategory");
    }

    //TC064_adminSuccessfulDeletesCategory()
    public void assertCategoryIsDeleted() {
        Utils.LOG.info("Check if category is deleted");

        assertElementNotPresent("ADMIN.EditDesiredCategory");
    }

    //TC065_adminSuccessfulDeletesPost()
    public void assertConnectionsPostIsDeleted() {
        Utils.LOG.info("Check if connection post is deleted");

        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        clickElement("ADMIN.Posts");
        isElementPresentUntilTimeout("ADMIN.ListPosts", 40);
        clearAndTypeValueInField(CONNECTIONS_TITLE_POST, "ADMIN.SearchKeywordInput");

        clickElement("ADMIN.SearchButton");
        isElementPresentUntilTimeout("ADMIN.SearchedPostForDeleting", 40);

        Utils.LOG.info("Xpath does NOT exist: " + "ADMIN.SearchedPostForDeleting");
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey("ADMIN.SearchedPostForDeleting"))).size());
    }

    public void assertUserIsSuccessfullyRegistered(){
        Utils.LOG.info("Check is registration is successful");

        isElementPresentUntilTimeout("REGISTRATION.RegisterLoginButton",30);
        assertElementPresent("REGISTRATION.RegisterLoginButton");
        isElementPresentUntilTimeout("REGISTRATION.RegisterLoginButton",30);
        assertElementPresent("REGISTRATION.SuccessRegistrationMessage");
    }

    public void assertUser2IsDisconnectedWithUser3(){
        Utils.LOG.info("Check if users are disconnected");

        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        clickElement("USERS.SectionUsers");
        isElementPresentUntilTimeout("USERS.ListUsers", 30);
        isElementPresentUntilTimeout("USERS.SearchedFriendDisconnecting2", 30);
        clickElement("USERS.SearchedFriendDisconnecting");
        assertElementPresent("USERS.ConnectButton");
    }
    public void assertUserInfoIsUpdated() {
        Utils.LOG.info("Check if user info is updated");

        assertElementPresent("UPDATED.Profile.Name");
        assertElementPresent("UPDATED.Profile.LastName");
        assertElementPresent("UPDATED.Profile.Age");
        assertElementPresent("UPDATED.Profile.Gender");
        assertElementPresent("UPDATED.Profile.Nationality");
        assertElementPresent("UPDATED.Profile.AdditionalInfo");

    }
}
