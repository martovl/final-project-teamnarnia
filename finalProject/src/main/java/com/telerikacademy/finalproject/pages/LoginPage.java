package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;


public class LoginPage extends BasePage {

    public LoginPage() {
        super("login.url");
    }


    //#############OPERATIONS WITH LOGIN #########

    public void logIn(String userName, String password) {
        assertPageNavigated();
        actions.isElementPresentUntilTimeout("loginPage.LoginButton", 30);
        actions.assertElementPresent("loginPage.LoginButton");
        actions.assertElementPresent("loginPage.EmailField");
        actions.isElementPresentUntilTimeout("loginPage.EmailField",30);
        actions.clearAndTypeValueInField(userName, "loginPage.EmailField");
        actions.assertElementPresent("loginPage.PasswordField");
        actions.isElementPresentUntilTimeout("loginPage.PasswordField",30);
        actions.clearAndTypeValueInField(password, "loginPage.PasswordField");
        actions.clickElement("loginPage.LoginButton");
    }

    public void logOut() {
        actions.isElementPresentUntilTimeout("navigation.LogOut",50);
        actions.clickElement("navigation.LogOut");
    }

    public void logInWithWrongPassword(){
        Utils.LOG.info("Performing Login method");
        String password = Constants.WRONG_USER_PASSWORD;

        logIn(Constants.EMAIL_USER1_ADMIN,password);
    }

    public void logInWithWrongEmail(){
        Utils.LOG.info("Performing Login method");
        String email = Constants.WRONG_USER_EMAIL;

        logIn(email,Constants.PASSWORD);
    }

    public void logInWithEmptyFieldForPassword(){
        Utils.LOG.info("Performing Login method");
        String email = Constants.EMAIL_USER1_ADMIN;
        String password = "";

        logIn(email,password);
    }

    public void logInWithEmptyFieldForEmail(){
        Utils.LOG.info("Performing Login method");
        String email = " ";
        String password = Constants.PASSWORD;

        logIn(email,password);
    }

    //############# ASSERTS #########

    public void assertUserIsLoggedIn() {
        actions.isElementPresentUntilTimeout("navigation.UsersMenu",30);
        actions.assertElementPresent("navigation.UsersMenu");
    }

    public void assertUserIsLoggedOut() {

        actions.isElementPresentUntilTimeout("loginPage.LoginButton", 30);
        actions.assertElementPresent("loginPage.LoginButton");
    }
}
