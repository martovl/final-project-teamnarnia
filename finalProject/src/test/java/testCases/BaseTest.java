package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.utils.HealthyFoodActions;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
	UserActions actions = new UserActions();
	HealthyFoodActions healthyFoodActions = new HealthyFoodActions();
	@BeforeClass()
	public static void setUp(){
		UserActions.loadBrowser("base.url");
	}



	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
