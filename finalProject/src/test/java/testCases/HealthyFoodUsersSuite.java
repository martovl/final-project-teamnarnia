package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.net.URISyntaxException;

import static com.telerikacademy.finalproject.utils.Constants.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class HealthyFoodUsersSuite extends BaseTest {
    LoginPage loginPage = new LoginPage();


    @Before
    public void loginPageNavigate() {
        loginPage.navigateToPage();

    }

    @After
    public void logout() {
        loginPage.logOut();
        loginPage.assertUserIsLoggedOut();

    }

    // TC028_memberSuccessfulSendsRequest()
    // TC029_memberSuccessfulAcceptRequest
    @Test
    public void TC028_memberSuccessfulSendsRequest_AND_TC029_memberSuccessfulAcceptRequest() {

        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.sentUserConnectionRequest();
        healthyFoodActions.assertRequestIsSent();

        loginPage.logOut();
        loginPage.assertUserIsLoggedOut();

        loginPage.logIn(EMAIL_USER2_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.acceptUserConnectionRequest();

        healthyFoodActions.assertRequestIsAccepted();
    }

    @Test
    public void TC030_memberSuccessfulDisconnectsFriend() {

        loginPage.logIn(EMAIL_USER5_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.userDisconnectsFriend();

        healthyFoodActions.assertUsersAreDisconnected();
    }

    @Test
    public void TC043_memberSuccessfulUpdatesFirstNameANDLastNameAndAdditionalInformationForItself() {

        loginPage.logIn(EMAIL_USER5_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.updateUsersInfo();

        healthyFoodActions.assertUserInfoIsUpdated();
    }

    @Test
    public void TC049_memberSuccessfulChangesProfilePictureJPEGFormat() throws URISyntaxException {
        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.updateUserProfilePicture();

        healthyFoodActions.assertProfilePictureIsChanged();
    }

}
