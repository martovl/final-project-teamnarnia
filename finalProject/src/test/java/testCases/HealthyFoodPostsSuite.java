package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.net.URISyntaxException;

import static com.telerikacademy.finalproject.utils.Constants.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)


public class HealthyFoodPostsSuite extends BaseTest {
    LoginPage loginPage = new LoginPage();

    @Before
    public void loginPageNavigate() {
        loginPage.navigateToPage();

    }

    @After
    public void logout() {
        loginPage.logOut();
        loginPage.assertUserIsLoggedOut();

    }

    //TC_040 MemberSuccessfulCreatesConnectionsPostWithDescription
    //TC_065 AdminSuccessfulDeletesConnectionPost
    @Test
    public void TC040_memberSuccessfulCreatesConnectionsPostWithDescription_AND_TC065_adminSuccessfulDeletesConnectionPost()
            throws URISyntaxException {

        loginPage.logIn(EMAIL_USER2_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.userCreatesConnectionsPost();
        healthyFoodActions.assertPostIsCreated();

        loginPage.logOut();
        loginPage.assertUserIsLoggedOut();

        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.deleteAdminConnectionsPost();
        healthyFoodActions.assertConnectionsPostIsDeleted();

    }

    @Test
    public void TC054_memberSuccessfulLikesFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.userLikesFriendPost();

        healthyFoodActions.assertFriendPostIsLiked();

    }

    @Test
    public void TC056_memberSuccessfulDislikesFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.userDislikesFriendPost();
        healthyFoodActions.assertFriendPostIsDisliked();

    }

    @Test
    public void TC058_memberSuccessfulCommentsFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.userCommentFriendPost();
        healthyFoodActions.assertFriendPostIsCommented();

    }


    // TC_063 AdminSuccessfulCreatesNewCategory
    // TC_064 AdminSuccessfulDeletesCategory
    @Test
    public void TC063_adminSuccessfulCreatesNewCategory_AND_TC_064_adminSuccessfulDeletesCategory()
            throws URISyntaxException {

        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        healthyFoodActions.createAdminCategory();
        healthyFoodActions.assertCategoryIsCreated();

        healthyFoodActions.deleteAdminCategory();
        healthyFoodActions.assertCategoryIsDeleted();

    }
}
