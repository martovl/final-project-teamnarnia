package stepdefinitions;


import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.HealthyFoodActions;
import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import java.net.URISyntaxException;


public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();
    LoginPage loginPage = new LoginPage();
    HealthyFoodActions healthyFoodActions = new HealthyFoodActions();


    //go to homepage
    @Given("I am on login page")
    @When("I am on login page")
    @Then("I am on login page")
    public void goToLoginPage() {
        loginPage.navigateToPage();
    }

    //log in
    @Given("As a registered user I am logging in with $username and $password")
    @When("As a registered user I am logging in with $username and $password")
    @Then("As a registered user I am logging in with $username and $password")
    public void logIn() {
        loginPage.logIn(Constants.EMAIL_USER3_REGULAR, Constants.PASSWORD);
    }

    @Given("I am clicking on registration button")
    @When("I am clicking on registration button")
    @Then("I am clicking on registration button")
    public void clickRegistrationButton() {
        actions.clickElement("loginPage.RegisterButton");
    }

    @Given("Wait $element element to be present")
    @When("Wait $element element to be present")
    @Then("Wait $element element to be present")
    public void isElementPresentUntilTimeout(String element) {
        actions.isElementPresentUntilTimeout(element, 30);
    }


    @Given("I fill all required fields")
    @When("I fill all required fields")
    @Then("I fill all required fields")
    public void fillRegistrationForm() throws URISyntaxException {
        healthyFoodActions.fillRegistrationForm();
    }


    @Given("I logout from my profile")
    @When("I logout from my profile")
    @Then("I logout from my profile")
    public void logOut() {
        loginPage.logOut();
    }

    @Given("I am disconnecting $User2 from my friends list")
    @When("I am disconnecting $User2 from my friends list")
    @Then("I am disconnecting $User2 from my friends list")
    public void disconnectUsers() {
        healthyFoodActions.disconnectUser2();
    }
    //Assertions
    @Then("I assert presence of registration form")
    public void assertRegistrationFormIsVisible() {
        actions.assertElementPresent("REGISTRATION.Header");
    }

    @Then("I assert I am logged in successfully in my profile")
    public void assertUserIsLogged() {
        loginPage.assertUserIsLoggedIn();
    }

    @Then("I assert I am logged out from my profile")
    public void assertUserIsLogOut() {
        loginPage.assertUserIsLoggedOut();
    }

    @Then("I assert presence of successful registration message")
    public void assertRegistrationIsSuccessful() {
        healthyFoodActions.assertUserIsSuccessfullyRegistered();
    }

    @Then("I assert I am not connected with $User2 anymore")
    public void assertUsersAreDisconnected() {
        healthyFoodActions.assertUser2IsDisconnectedWithUser3();
    }

}
