Meta:
@registration

Narrative:

As a registered user
I want to login in my profile
So that I can log out


Scenario: Login and logout with valid credentials
Given I am on login page
When As a registered user I am logging in with username and password
Then I assert I am logged in successfully in my profile
And I logout from my profile
Then I assert I am logged out from my profile





