Meta:
@registration

Narrative:

As a user
I want to fill registration form
So that I can become member of the Social Network


Scenario: New user register successfully
Given I am on login page
When I am clicking on registration button
Then I assert presence of registration form
And I fill all required fields
Then I assert presence of successful registration message








