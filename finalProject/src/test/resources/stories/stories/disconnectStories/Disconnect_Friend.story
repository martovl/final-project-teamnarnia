Meta:
@registration

Narrative:

As a register user
I want to disconnect with user I am connected
So I am not going to be in their friend list anymore


Scenario: Two user are disconnecting
Given I am on login page
When As a registered user I am logging in with User3  and PASSWORD
And I am disconnecting User2 from my friends list
Then I assert I am not connected with User2 anymore
