-- --------------------------------------------------------
-- Host:                         narniaqa22.online
-- Server version:               10.3.22-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for information_schema

-- Dumping database structure for narnia232_project
CREATE DATABASE IF NOT EXISTS `narnia232_project` /*!40100 DEFAULT CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci */;
USE `narnia232_project`;

-- Dumping structure for table narnia232_project.authorities
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(100) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `authorities_pk` (`username`,`authority`),
  CONSTRAINT `authorities_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.authorities: ~69 rows (approximately)
DELETE FROM `authorities`;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('10@abv.bg', 'ROLE_USER'),
	('10@gmail.com', 'ROLE_USER'),
	('11@gmail.com', 'ROLE_USER'),
	('11@outlook.com', 'ROLE_USER'),
	('12@gmail.com', 'ROLE_USER'),
	('1@abv.bg', 'ROLE_USER'),
	('1@gmail.com', 'ROLE_USER'),
	('1@outlook.com', 'ROLE_USER'),
	('1@yahoo.com', 'ROLE_USER'),
	('4@gmail.com', 'ROLE_USER'),
	('a@gmail.com', 'ROLE_USER'),
	('aaa@abv.bg', 'ROLE_USER'),
	('aaaa@abv.bg', 'ROLE_USER'),
	('AbXFJMJDVE@gmail.com', 'ROLE_USER'),
	('ArjsKQxvvh@gmail.com', 'ROLE_USER'),
	('b@gmail.com', 'ROLE_USER'),
	('bbb@gmail.com', 'ROLE_USER'),
	('bpQXz@gmail.com', 'ROLE_USER'),
	('bRcPe@gmail.com', 'ROLE_USER'),
	('BvXtj@gmail.com', 'ROLE_USER'),
	('c@gmail.com', 'ROLE_USER'),
	('ccc@gmail.com', 'ROLE_USER'),
	('CZtxipdhBC@gmail.com', 'ROLE_USER'),
	('d@gmail.com', 'ROLE_USER'),
	('deleted_user@gmail.com', 'ROLE_USER'),
	('DlOYi@gmail.com', 'ROLE_USER'),
	('EASHi@gmail.com', 'ROLE_USER'),
	('ee@gmail.com', 'ROLE_USER'),
	('egcaF@gmail.com', 'ROLE_USER'),
	('evaHF@gmail.com', 'ROLE_USER'),
	('finalprojectnarnia@gmail.com', 'ROLE_ADMIN'),
	('finalprojectnarnia@gmail.com', 'ROLE_USER'),
	('fyAMmBmyjh@gmail.com', 'ROLE_USER'),
	('hFPwn@gmail.com', 'ROLE_USER'),
	('hsgMMLWAxY@gmail.com', 'ROLE_USER'),
	('irka_1982@abv.bg', 'ROLE_USER'),
	('JXYcQ@gmail.com', 'ROLE_USER'),
	('KHCXP@gmail.com', 'ROLE_USER'),
	('KNtYd@gmail.com', 'ROLE_USER'),
	('kZqYx@gmail.com', 'ROLE_USER'),
	('LEfTZ@gmail.com', 'ROLE_USER'),
	('LGiNV@gmail.com', 'ROLE_USER'),
	('LopPL@gmail.com', 'ROLE_USER'),
	('LQAkW@gmail.com', 'ROLE_USER'),
	('LUCVS@gmail.com', 'ROLE_USER'),
	('martin.vlachkov22@gmail.com', 'ROLE_USER'),
	('marto221@gmail.com', 'ROLE_USER'),
	('marto22@gmail.com', 'ROLE_USER'),
	('marto@abv.bg', 'ROLE_USER'),
	('mjOvHbncFm@gmail.com', 'ROLE_USER'),
	('nAKPV@gmail.com', 'ROLE_USER'),
	('nevenamalinova@outlook.com', 'ROLE_USER'),
	('nevina_82@abv.bg', 'ROLE_USER'),
	('nluVw@gmail.com', 'ROLE_USER'),
	('NPwaOvqnCg@gmail.com', 'ROLE_USER'),
	('NWpDv@gmail.com', 'ROLE_USER'),
	('oeweL@gmail.com', 'ROLE_USER'),
	('PkkXd@gmail.com', 'ROLE_USER'),
	('qTkUK@gmail.com', 'ROLE_USER'),
	('renbSpQMCV@gmail.com', 'ROLE_USER'),
	('RQwxu@gmail.com', 'ROLE_USER'),
	('sUFFkfpNIB@gmail.com', 'ROLE_USER'),
	('test1234@gmail.com', 'ROLE_USER'),
	('test123@gmail.com', 'ROLE_USER'),
	('tpOXs@gmail.com', 'ROLE_USER'),
	('UbyKN@gmail.com', 'ROLE_USER'),
	('uFJiXukDPB@gmail.com', 'ROLE_USER'),
	('vwHMvSpcbp@gmail.com', 'ROLE_USER'),
	('WuLgk@gmail.com', 'ROLE_USER'),
	('xaKyM@gmail.com', 'ROLE_USER'),
	('XWjKp@gmail.com', 'ROLE_USER'),
	('YEMAiljUOO@gmail.com', 'ROLE_USER'),
	('YzMPKrIgXZ@gmail.com', 'ROLE_USER'),
	('ZeXet@gmail.com', 'ROLE_USER'),
	('ztSDA@gmail.com', 'ROLE_USER'),
	('ZxFam@gmail.com', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `emoji` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `emoticons_emoji_uindex` (`emoji`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.categories: ~142 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `emoji`, `enabled`, `name`) VALUES
	(1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603705399/orinej3qkkb5hz9b459s.png', 1, 'Children food'),
	(2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603706769/nysvx7doutn62nopitcv.png', 0, 'Drinks'),
	(3, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1604918862/kwjdgpcqskszxmby40rw.jpg', 0, 'Breakfast'),
	(4, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605399864/pemqv1teqve3dpyy0fng.png', 0, '@@@@@'),
	(5, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605823053/tjljhlq6vzntqhj03hit.png', 0, 'Children drinks'),
	(6, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605865032/rierotqzkjgno9lnexeo.png', 0, '11'),
	(7, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605865752/qqffmszj1ewe64ismavl.png', 0, 'Children drinks'),
	(8, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605868358/aydopbrgu72i1izzgwtn.png', 0, '12'),
	(9, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605870196/osf2t9y6yvfcokpndydf.png', 0, 'Children drinks'),
	(10, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605870321/ypzlfdww2w163ymcsyws.png', 0, 'Children drinks'),
	(11, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605871017/wacn3tr9duv0wxehqybt.png', 0, 'Children drinks'),
	(12, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605871440/as1mkv2xmx4jfuhxzfpi.png', 0, 'Children drinks'),
	(13, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605871978/yyxakjnqpmqdlncdh75n.png', 0, 'Children drinks'),
	(14, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605872951/so6ixthbw0lbgra5hdix.png', 0, 'Children drinks'),
	(15, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605877990/qpzdqwitxbiesrs43tjz.png', 0, 'Children drinks'),
	(16, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605878845/gjdruy3ogciilfnvmfi2.png', 0, 'Children drinks'),
	(17, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605879061/azvaftyzy8ofvtuy8jkg.png', 0, 'Children drinks'),
	(18, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605879468/fbm4h0tjujljjwty3ayj.png', 0, 'Children drinks'),
	(19, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605879793/hbvgcf3bb5ukqcwfc4eh.png', 0, 'Children drinks'),
	(20, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605885106/hq0eha3hyiuny2ishrr9.png', 0, 'Children drinks'),
	(21, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605904453/u49os76rdqqdehwh0dow.jpg', 0, 'Children drinks'),
	(22, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605906607/gp4se5gsbgmhdnkoiaad.png', 0, 'Children drinks'),
	(23, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605906851/btz2mxpfdfdl8dzhvx7v.png', 0, 'Children drinks'),
	(24, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605907191/dzlmqnlaweof6xchlxga.png', 0, 'Children drinks'),
	(25, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605907423/k8me82s9ruq0aftfo1hj.png', 0, 'Children drinks'),
	(26, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605907673/mcfl2x5cibhjsqefml06.png', 0, 'Children drinks'),
	(27, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605907841/qceyofvyfsphwssdjzv7.png', 0, 'Children drinks'),
	(28, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605908555/xt2rcixdvffrya1fuz1y.png', 0, 'Children drinks'),
	(29, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605908655/nkxrxcsuqdo6h7fd0my8.png', 0, 'Children drinks'),
	(30, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605908961/csuo1docyiaruqyqbiet.png', 0, 'Children drinks'),
	(31, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605909064/pnrucctgujt9fp3iq4f2.png', 0, 'Children drinks'),
	(32, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605909183/kxah5nuvnkevz4stzaat.png', 0, 'Children drinks'),
	(33, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605909475/kuetiazyqus1dsgmlb3h.png', 0, 'Children drinks'),
	(34, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605909922/f8bqgofluuzkgxwgpn64.png', 0, 'Children drinks'),
	(35, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605910233/zvk8vfbihbqwjbctgh1l.png', 0, 'Children drinks'),
	(36, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605910436/ixuqmizrcdq96rdupotz.png', 0, 'Children drinks'),
	(37, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605910596/ffkhm4izbqpgpxdpbhyq.png', 0, 'Children drinks'),
	(38, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605910973/cemeah6ogblu09cnes8g.png', 0, 'Children drinks'),
	(39, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605911531/ei3afxufzzatvvb961ey.png', 0, 'Children drinks'),
	(40, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605912788/qhx80p0kvucxdw3vijmm.png', 0, 'Children drinks'),
	(41, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605912945/mghp29e7cc1sc375c2bh.png', 0, 'Children drinks'),
	(42, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605913795/dec4s5esa56fnxixpudm.png', 0, 'Children drinks'),
	(43, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605913974/uwtagrl47vwwcui57om1.jpg', 0, 'Children drinks'),
	(44, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605947251/wwx3tnrlvyymppydhyep.png', 0, 'Children drinks'),
	(45, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605948116/fczwhljnuoatpebi5lly.png', 0, 'Children drinks'),
	(46, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605948739/wrlumrqyzdynhrb5zkri.png', 0, 'Children drinks'),
	(47, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605948978/adf4c0mt9qisvicxb5s1.png', 0, 'Children drinks'),
	(48, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605949217/fbb0tyx0zfzhniexuwm9.png', 0, 'Children drinks'),
	(49, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605949337/zabyww1n4hwosigqy1uw.png', 0, 'Children drinks'),
	(50, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605949472/z1piks4ejmqroglqneal.png', 0, 'Children drinks'),
	(51, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605949597/wkfepzsngmnq8182qnyx.png', 0, 'Children drinks'),
	(52, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605950036/x2sdekwf9gzwhj6lxjne.png', 0, 'Children drinks'),
	(53, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605950115/nxrsqjyu7kkr2wfs8nnv.png', 0, 'Children drinks'),
	(54, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605950261/a3zqwbkp2biqtygztyci.jpg', 0, 'Children drinks'),
	(55, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605950442/rk17sujck5jtwplj0xqi.png', 0, 'Children drinks'),
	(56, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605950549/zwy5om546dbizl8ttfm3.png', 0, 'Children drinks'),
	(57, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605951543/b04gu9apoumprlnndppg.png', 0, 'Children drinks'),
	(58, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605951756/jm5i17wziq5emo8el3ia.png', 0, 'Children drinks'),
	(59, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605951915/tfyyyfogxuasjcu4ul2d.png', 0, 'Children drinks'),
	(60, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605952217/sf6zw6m1dnuhsussggyn.png', 0, 'Children drinks'),
	(61, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605952438/hznqlczcol83z3jtcr88.png', 0, 'Children drinks'),
	(62, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605952567/mwulhzyhdslbvqv6o0el.png', 0, 'Children drinks'),
	(63, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605957953/ukkagg4hvanvvlszrvap.png', 0, 'Children drinks'),
	(64, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605961064/ugoxf2crabkwlpg3uzf5.png', 0, 'Children drinks'),
	(65, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605961509/asupros7tcngkegql44d.png', 0, 'Children drinks'),
	(66, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994596/rrrbay4ygtpa4t2jz87l.png', 0, 'Children drinksChildren drinks'),
	(67, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994689/zacnyoax4fr14e06nfm9.png', 0, 'Children drinks'),
	(68, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605995321/clitvxnaduabsuho3kvq.png', 0, 'Children drinks'),
	(69, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605996268/w82nzrtgs9uhfb9amkzm.png', 0, 'Children drinks'),
	(70, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605996860/g56yw8azgt4kmyujxzzf.png', 0, 'Children drinks'),
	(71, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605997249/o4dxbbzncxbpxdehifzw.png', 0, 'Children drinks'),
	(72, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605997824/khv9emp6ztliwcf5awkz.png', 0, 'Children drinks'),
	(73, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605997945/guownnlxqosdt02pru7f.png', 0, 'Children drinks'),
	(74, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998085/pgh6warzqgvg1z7kzq3p.png', 0, 'Children drinks'),
	(75, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998153/oklpkddlcftgcqdacbbz.png', 0, 'Children drinks'),
	(76, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998297/y5isn16d1uqjng3hopt3.png', 0, 'Children drinks'),
	(77, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998362/ad9pczixxkd5dpt4owvu.png', 0, 'Children drinks'),
	(78, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998427/gvnm8fq7ijbjde6d1fov.png', 0, 'Children drinks'),
	(79, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605998489/nmrjwnilbemdbs9dvktw.png', 0, 'Children drinks'),
	(80, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605999022/tggahivcesnoe6nbluc4.png', 0, 'Children drinks'),
	(81, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606036008/zd2mcesncj9nluukwosp.png', 0, 'Children drinks'),
	(82, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606041219/dhs6ndq9n9sk67sby8w6.png', 0, 'Children drinks'),
	(83, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606041656/wvvrhsplsuli94vokzax.png', 0, 'Children\'s drink'),
	(84, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606041841/hq8pcxqygxekaufxackr.png', 0, 'Children drinks'),
	(85, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606049215/mlbrmiodeu6qp2fjafza.png', 0, 'Children drinks'),
	(86, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606082145/zhymbnegiecsevd76hh4.png', 0, 'Children drinks'),
	(87, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606084725/zutfhzh6bhx7sg2mppgz.png', 0, 'Children drinks'),
	(88, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606084852/zkxpie6bnyjn8avytlhj.png', 0, 'Children drinks'),
	(89, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606086635/wk0wrwaq8wiidkuektty.png', 0, 'Children drinks'),
	(90, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606086822/ahfqfacdjlrgrtcxy5kv.png', 0, 'Children drinks'),
	(91, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606087173/jkhzb61wqvtvnvgevkwc.jpg', 0, 'Children drinks'),
	(92, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606091171/l7zvkn9v5hwjdvanra7m.jpg', 0, 'Children drinks'),
	(93, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606091381/aphxhkhgg5nfdnbl03uj.png', 0, 'Children drinks'),
	(94, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606091565/cpk9drbugnmtuk3wud89.png', 0, 'Children drinks'),
	(95, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606124502/svygrzd3ooebuqfdjbze.png', 0, 'Children drinks'),
	(96, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165346/tjzf2n62apeo5xudnvku.png', 0, 'Children drinks'),
	(97, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165826/xq4wa4ucjsypebhncfij.png', 0, 'Children drinks'),
	(98, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606168646/drywaarn9xenruw87prc.png', 0, 'Children drinks'),
	(99, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606168915/dccta1q1uanjewsmu8am.png', 0, 'Children drinks'),
	(100, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606169046/ngqlkji7oliec8abii1s.png', 0, 'Children drinks'),
	(101, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606252219/sao93jumtdbkye0kglqd.jpg', 0, 'Desserts'),
	(102, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606254077/o66dqp7r101wdqrby4bb.jpg', 0, 'Desserts'),
	(103, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606255234/v4ougobbsvjpajpa56nr.jpg', 0, 'Desserts'),
	(104, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606255297/iwsoxiyfqodbalco2mlf.jpg', 0, 'Desserts'),
	(105, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606255447/rp5djvxugctvmpgkksem.jpg', 0, 'Desserts'),
	(106, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606290680/vnjttvxct5quaexi0lit.jpg', 0, 'Children drinks'),
	(107, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606312972/alexnfst0vosolcmbk5y.png', 0, 'Children drinks'),
	(108, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606313710/znjtvlccacigckq21bdq.jpg', 0, 'Children drinks'),
	(109, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606320116/hsiht7exfnpsfonlfm7u.jpg', 0, 'Children drinks'),
	(110, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606320299/slii8tilt2droxzbdomi.jpg', 0, 'Children drinks'),
	(111, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606320405/iz8gu9sqyztrgow2ebgb.jpg', 0, 'Children drinks'),
	(112, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606321097/oeofnsctyq0adboqatzy.jpg', 0, 'Children drinks'),
	(113, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606322176/b1makwehrwdumezdwydj.jpg', 0, 'Children drinks'),
	(114, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606464572/pttkrhu1ok7c1bymdwaf.jpg', 0, 'Children drinks'),
	(115, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490550/synukcevnvzesgwak4l8.jpg', 0, 'Children drinks'),
	(116, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490628/szrkzqgsgrvo0dsg2w9g.jpg', 0, 'Children drinks'),
	(117, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606498557/qmxv3hzda2oyii3noggt.jpg', 0, 'Children drinks'),
	(118, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499033/fgbzl3vuq4zwhc07jdrt.jpg', 0, 'Children drinks'),
	(119, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499112/i4u5daes9dmv0dh4to3q.jpg', 0, 'Children drinks'),
	(120, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499191/bwwijbahz88pablot6z7.jpg', 0, 'Children drinks'),
	(121, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499885/f5hdklsknpgg9qwybsz1.jpg', 0, 'Children drinks'),
	(122, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606500124/zk58oliyiozubpsha58t.jpg', 0, 'Children drinks'),
	(123, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606500621/kmnr5fukbsqfc6u41zv3.jpg', 0, 'Children drinks'),
	(124, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606500714/c4ss04ustigqaolgo3db.jpg', 0, 'Children drinks'),
	(125, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606501531/s97amqrlctmenlddxn8b.jpg', 0, 'Children drinks'),
	(126, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606502941/tyzkdblhl65usgf6ozwd.jpg', 0, 'Children drinks'),
	(127, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503026/m6fef0fouytewusyvhmp.jpg', 0, 'Children drinks'),
	(128, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503107/ahrswjbrljpm7povqhe2.jpg', 0, 'Children drinks'),
	(129, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503256/mjtwsfgzhqjbyqryrlqw.jpg', 0, 'Children drinks'),
	(130, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503340/bqn6yo4liby2utqaseux.jpg', 0, 'Children drinks'),
	(131, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503478/ptjv89r4uugdnlvtttnf.jpg', 0, 'Children drinks'),
	(132, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503558/r96zmawvlpgu2r4qfhj6.jpg', 0, 'Children drinks'),
	(133, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503668/v4agdw8kb5m8xz38gesi.jpg', 0, 'Children drinks'),
	(134, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503992/tgk1dwqkihfawp62vc9i.jpg', 0, 'Children drinks'),
	(135, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606505402/api5muwyjxbexz9fee5y.jpg', 0, 'Children drinks'),
	(136, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606511720/d6ifoazivhalkmko7kpm.png', 0, 'Children drinks'),
	(137, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606577849/mxyq6nnmbd7ewydjnuyq.jpg', 0, 'Children drinks'),
	(138, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606577914/uo6bgqe9mdcugn2gebl0.jpg', 0, 'Children drinks'),
	(139, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606590485/kza3hvln3ep6jy0mwpj2.jpg', 0, 'Children drinks'),
	(140, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606596482/aq8wicbsc2mcgkdviqja.jpg', 0, 'Children drinks'),
	(141, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606599311/q9augabrblquwno7vdgx.png', 0, 'Children drinks'),
	(142, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606600923/dmwplvblshnstjqi06wl.png', 0, 'Children drinks');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `creator_id` bigint(20) NOT NULL,
  `description` longtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`comment_id`),
  KEY `FKb4djmnbg40e6hai3awn0btuso` (`creator_id`),
  KEY `FKh4c7lvsc298whoyd4w9ta25cr` (`post_id`),
  CONSTRAINT `FKb4djmnbg40e6hai3awn0btuso` FOREIGN KEY (`creator_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `FKh4c7lvsc298whoyd4w9ta25cr` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `comments_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `comments_users_details_user_details_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `users_details` (`user_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.comments: ~63 rows (approximately)
DELETE FROM `comments`;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES
	(1, 5, 2, 'It is true!', '2020-10-26 14:29:10', 1),
	(2, 6, 1, 'It is true!', '2020-10-26 14:31:09', 1),
	(3, 6, 1, 'It is true!', '2020-10-26 14:31:13', 0),
	(4, 6, 1, 'I love sweet potato', '2020-10-26 14:33:13', 1),
	(5, 6, 2, 'I love sweet potato too :)', '2020-10-26 14:47:51', 1),
	(6, 7, 2, 'I love avocado! Mariah Carey :)', '2020-10-26 15:36:26', 1),
	(7, 7, 3, 'I love avocado, too! Irina', '2020-10-26 15:38:50', 1),
	(8, 7, 1, 'Thank you!', '2020-10-27 15:23:35', 1),
	(9, 11, 4, 'I like sweet potato!', '2020-10-30 12:48:10', 1),
	(10, 11, 1, 'I like sweet potato, too!', '2020-10-30 12:51:34', 1),
	(11, 11, 2, 'Yeah, sweet potato :)', '2020-10-30 12:54:39', 1),
	(12, 11, 2, 'I like to cook sweet potato!', '2020-10-30 12:58:09', 1),
	(13, 11, 2, 'I often cook sweet potato!', '2020-10-30 12:59:39', 1),
	(14, 7, 1, 'Avocado', '2020-10-30 13:02:20', 1),
	(15, 14, 5, 'Really interesting post!', '2020-11-09 11:06:45', 1),
	(16, 14, 1, 'I like your image!', '2020-11-09 12:31:16', 1),
	(17, 27, 1, '1', '2020-11-15 23:34:54', 0),
	(18, 27, 1, '1', '2020-11-15 23:34:58', 0),
	(19, 27, 1, 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England. However, there is upland and mountainous terrain in the north (for example, the Lake District and Pennines) and in the west (for example, Dartmoor and the Shropshire Hills). The capital is London, which has the largest metropolitan area in both the United Kingdom and, prior to Brexit, the European Union.[nb 1] England\'s population of over 55 million comprises 84% of the population of the United Kingdom,[5] largely concentrated around London, the South East, and conurbations in the Midlands, the North West, the North East, and Yorkshire, which each developed as major industrial regions during the 19th century.[13]', '2020-11-15 23:39:43', 0),
	(20, 27, 1, 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England. However, there is upland and mountainous terrain in the north (for example, the Lake District and Pennines) and in the west (for example, Dartmoor and the Shropshire Hills). The capital is London, which has the largest metropolitan area in both the United Kingdom and, prior to Brexit, the European Union.[nb 1] England\'s population of over 55 million comprises 84% of the population of the United Kingdom,[5] largely concentrated around London, the South East, and conurbations in the Midlands, the North West, the North East, and Yorkshire, which each developed as major industrial regions during the 19th century.[13]\r\nThe Kingdom of England – which after 1535 included Wales – ceased being a separate sovereign state on 1 May 1707, when the Acts of Union put into effect the terms agreed in the Treaty of Union the previous year, resulting in a political union with the Kingdom of Scotland to create the Kingdom of Great Britain.[14][15] In 1801, Great Britain was united with the Kingdom of Ireland (through another Act of Union) to become the United Kingdom of Great Britain and Ireland. In 1922 the Irish Free State seceded from the United Kingdom, leading to the latter being renamed the United Kingdom of Great Britain and Northern Ireland.[16]mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmkjiajfip\r\nvndhviodjvpidjvpiadejpa\'up\\\r\nkdnvaieja\'ojoaejboapegi[ae7\r\nvjkdabvalifjaijfpaojfIKFA"PFoa\r\nkjm n jk n;ped ;;\'kmk,,admnvjnoef[aofap\'lv[aeo[a]poef[cl[sdflllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllnvisfjbsip\\bjposbjosp\\opejgvojdoejbojsdbojsfbj\\sj;djsbk;sjndbdsbnsdjbnsdnbkdsnbdkn s sibjsdvjbdjbosjvbodsjvodajboadjvboaj\\vbo\\ajdoejvoaejvoajboajdbojao\\jbaeoaeojeoeajboejeojaeojeaojeeeiojeiujuwugr8gur8ygrighrygrhgorihvdfjn bdughiaworugh\';WEUF\'qifpeof\'KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKMBRIEWAJGRAPWIRGJAWJGIPWRJGIPAWRJHAIPHJPARIJHBPIREOJHBPAEIRHJBAEROIPHJPEIRJHPEAIRJHPIERJHPIETJHBETIHJAEIPTHJAEPIHAPIERHAOWRIGWARPIGPWROHJIOPRJBPOEAIGHEPARGIAWRI9-RUHJA\\]0WI4T0-AWHJA0W-4I0HUJ0AWI4GA0W4IHG0W4IG04HUJ04IY04WAJNH04T94=090JJGLRSLRPOOKPPPPPPPPPPPGL4W0OT=-4O0V96YITIK\\S[0OT5Y\r\nW-45HOHK604940-650IHLOHKB50Y945Y95Y9HOHIIHH0HPHPHPPHPHPHPHPHPHPPHPHPHPPHPHPOH-4------NMOYOOOOMN\'WKHW4IH-4WI50IYH0-54IY045IY0I064IH046IH40YHIY046HIIHIIMNYOHI469IYH4-6IH40IY40Y9045HILBKJHT9IY645HJDIOIYOH,OOOOOOOOOOOOOOHK046YIH045Y94=9Y0TIH0IRT-HI4HI05HI4HIW45YI460YHI46HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH', '2020-11-15 23:43:39', 0),
	(21, 27, 1, '1', '2020-11-15 23:52:08', 0),
	(22, 27, 1, '1', '2020-11-15 23:54:43', 0),
	(23, 27, 1, '1', '2020-11-16 00:03:58', 0),
	(24, 27, 4, 'ha ha', '2020-11-16 00:33:05', 1),
	(25, 27, 4, '12', '2020-11-16 00:59:23', 1),
	(26, 27, 4, '33', '2020-11-16 01:00:10', 1),
	(27, 2, 3, 'Hello', '2020-11-23 17:23:33', 0),
	(28, 2, 3, 'This is awesome post!', '2020-11-23 17:30:13', 0),
	(29, 2, 3, 'This is awesome post!', '2020-11-23 22:09:31', 0),
	(30, 2, 1, 'This is awesome post', '2020-11-23 22:36:54', 0),
	(31, 2, 3, 'This is awesome post!', '2020-11-23 23:01:59', 0),
	(32, 2, 3, 'This is awesome post!', '2020-11-23 23:09:53', 0),
	(33, 2, 3, 'This is awesome post!', '2020-11-23 23:56:30', 0),
	(34, 2, 1, 'I like vegetables', '2020-11-25 16:13:34', 1),
	(35, 2, 3, 'This is awesome post!', '2020-11-26 02:24:25', 0),
	(36, 141, 11, 'occaecati', '2020-11-26 23:31:18', 1),
	(37, 141, 11, 'sequi', '2020-11-26 23:34:55', 1),
	(38, 141, 11, 'commodi', '2020-11-26 23:35:56', 1),
	(39, 141, 11, 'quae', '2020-11-26 23:36:30', 0),
	(40, 148, 1, 'ut', '2020-11-27 14:01:28', 0),
	(41, 149, 1, 'minus', '2020-11-27 15:21:04', 0),
	(42, 150, 1, 'a', '2020-11-27 15:48:08', 0),
	(43, 152, 1, 'a', '2020-11-27 15:56:05', 1),
	(44, 153, 1, 'a', '2020-11-27 15:57:43', 1),
	(45, 154, 1, 'qui', '2020-11-27 15:59:04', 0),
	(46, 156, 1, 'dolores', '2020-11-27 17:22:36', 0),
	(47, 158, 1, 'est', '2020-11-27 17:23:54', 0),
	(48, 160, 1, 'assumenda', '2020-11-27 19:36:02', 0),
	(49, 162, 1, 'provident', '2020-11-27 19:43:58', 0),
	(50, 164, 1, 'ex', '2020-11-27 19:45:17', 0),
	(51, 166, 1, 'dicta', '2020-11-27 19:46:35', 0),
	(52, 168, 1, 'ut', '2020-11-27 20:54:23', 0),
	(53, 170, 1, 'rerum', '2020-11-27 20:55:47', 0),
	(54, 172, 1, 'voluptas', '2020-11-27 20:58:04', 0),
	(55, 174, 1, 'assumenda', '2020-11-27 20:59:25', 0),
	(56, 176, 1, 'doloribus', '2020-11-27 21:01:13', 0),
	(57, 178, 1, 'perferendis', '2020-11-27 21:06:37', 0),
	(58, 180, 1, 'harum', '2020-11-27 21:30:07', 0),
	(59, 182, 1, 'fugit', '2020-11-28 17:38:40', 0),
	(60, 184, 1, 'velit', '2020-11-28 21:08:09', 0),
	(61, 186, 1, 'ipsa', '2020-11-28 22:48:08', 0),
	(62, 2, 3, 'This is awesome post!', '2020-11-28 23:34:41', 0),
	(63, 2, 3, 'This is awesome post!', '2020-11-29 00:01:35', 0);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.comments_likes
CREATE TABLE IF NOT EXISTS `comments_likes` (
  `like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_details_id` bigint(20) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  PRIMARY KEY (`like_id`),
  UNIQUE KEY `likes_pk_2` (`user_details_id`,`comment_id`),
  KEY `FKogmkq8clqlxqis53e9tlu4w96` (`comment_id`),
  CONSTRAINT `FKmpfiqs4t1s42lx562slxisv9u` FOREIGN KEY (`user_details_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `FKogmkq8clqlxqis53e9tlu4w96` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`comment_id`),
  CONSTRAINT `comments_likes_comments_comment_id_fk` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`comment_id`),
  CONSTRAINT `likes_users_details_user_details_id_fk` FOREIGN KEY (`user_details_id`) REFERENCES `users_details` (`user_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.comments_likes: ~8 rows (approximately)
DELETE FROM `comments_likes`;
/*!40000 ALTER TABLE `comments_likes` DISABLE KEYS */;
INSERT INTO `comments_likes` (`like_id`, `user_details_id`, `comment_id`) VALUES
	(3, 1, 6),
	(2, 1, 7),
	(9, 1, 8),
	(12, 1, 15),
	(8, 2, 8),
	(7, 3, 8),
	(11, 5, 13),
	(10, 5, 15);
/*!40000 ALTER TABLE `comments_likes` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.confirmation_tokens
CREATE TABLE IF NOT EXISTS `confirmation_tokens` (
  `confirmation_token_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY (`confirmation_token_id`),
  KEY `FKy55d7j11g08hd16e1r6ig2lp` (`username`),
  CONSTRAINT `FKy55d7j11g08hd16e1r6ig2lp` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `confirmation_tokens_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.confirmation_tokens: ~45 rows (approximately)
DELETE FROM `confirmation_tokens`;
/*!40000 ALTER TABLE `confirmation_tokens` DISABLE KEYS */;
INSERT INTO `confirmation_tokens` (`confirmation_token_id`, `confirmation_token`, `created_date`, `username`) VALUES
	(1, 'e6f95100-d2a4-4a1d-8095-a3ded5175385', '2020-10-25 20:59:56', 'finalprojectnarnia@gmail.com'),
	(2, '655d415e-b140-456c-a961-50e9df0d86fb', '2020-10-26 12:08:13', 'nevina_82@abv.bg'),
	(3, '8823d575-0abc-4df9-991b-b44f7165a1ea', '2020-10-26 14:16:22', 'irka_1982@abv.bg'),
	(4, 'ebe27d0b-edec-4ec0-a392-e0078f3c51eb', '2020-10-26 15:04:45', 'nevenamalinova@outlook.com'),
	(5, '537f11fa-e471-43a0-b205-b0a25cf9d043', '2020-11-02 11:34:14', 'martin.vlachkov22@gmail.com'),
	(6, 'b0ba9df6-845f-4f1b-b98e-271d4baf52ec', '2020-11-03 12:07:50', 'test123@gmail.com'),
	(7, 'acce6437-83e1-4701-9695-5c54a28a821b', '2020-11-10 18:15:11', '1@abv.bg'),
	(8, '49b5b167-f4c8-45e4-bcb7-1f267d19deb1', '2020-11-10 18:32:06', '1@outlook.com'),
	(9, '5dd4e565-5ecb-4326-b06d-96e911475974', '2020-11-10 18:38:01', '4@gmail.com'),
	(10, '6a28b9aa-87c6-4b1f-a18b-837f88f6a7b1', '2020-11-10 19:09:48', '1@gmail.com'),
	(11, '0a781452-dc71-466c-b9eb-8a413c54fdc7', '2020-11-10 22:50:15', '10@gmail.com'),
	(12, 'e749ea1d-01c8-426a-a7be-3bdfadeb9c8c', '2020-11-10 23:39:56', '11@gmail.com'),
	(13, '2313e2df-1032-4911-a53e-1bacbdf1cbd2', '2020-11-11 00:43:41', '12@gmail.com'),
	(14, 'd2af08bd-aac8-46f1-a4c9-f2cedf4b1b82', '2020-11-11 01:13:09', '10@abv.bg'),
	(15, 'd2778da6-fed6-4633-bffe-4ebff9fe0615', '2020-11-11 12:14:42', '11@outlook.com'),
	(16, '90b027a1-737a-4618-ba43-158b37f9bbb8', '2020-11-11 12:41:25', '1@yahoo.com'),
	(17, 'db2316c6-0bdc-4f83-9630-f17b31eb1d10', '2020-11-11 13:26:29', 'nevenamalinova@outlook.com'),
	(18, 'fb3f6569-255c-4697-9c04-222820b5f9ca', '2020-11-12 10:13:13', 'a@gmail.com'),
	(19, '711d404e-4483-4028-8237-d670cc839d77', '2020-11-12 10:15:20', 'b@gmail.com'),
	(20, 'ae5b7aab-8fd2-4643-88c6-a60b07986825', '2020-11-12 10:23:03', 'c@gmail.com'),
	(21, '2be1a201-9cf7-48b3-82b6-7ca0578eb504', '2020-11-12 10:29:38', 'd@gmail.com'),
	(22, 'b9cb878a-66b4-4139-9aea-9781ef7f4003', '2020-11-12 11:08:45', 'ee@gmail.com'),
	(23, '606b1de8-0d01-42ae-a80c-95f340295486', '2020-11-18 13:51:46', 'martin.vlachkov22@gmail.com'),
	(24, '0c9abbf4-e48f-466e-9633-a9447cfc7d3c', '2020-11-18 13:54:43', 'martin.vlachkov22@gmail.com'),
	(25, '47256534-e3c5-492d-894a-15e40094d055', '2020-11-19 20:34:12', 'martin.vlachkov22@gmail.com'),
	(26, '4eb078a0-a296-4080-8e6e-c2f6f790796e', '2020-11-19 21:00:06', 'martin.vlachkov22@gmail.com'),
	(27, '91d27ff1-cd02-43e3-b792-aed94d9c35ab', '2020-11-23 03:03:47', 'aaa@abv.bg'),
	(28, '2334179d-5e97-46c3-82b2-352c83590167', '2020-11-23 03:04:27', 'aaaa@abv.bg'),
	(29, '8d6b803e-80c8-4b1d-9577-c222cf11df92', '2020-11-23 12:44:43', 'bbb@gmail.com'),
	(30, '90833f84-a664-4243-bb20-51033a2a6b13', '2020-11-23 14:10:55', 'ccc@gmail.com'),
	(31, 'e6d8f4df-d1a7-45a7-97dd-82553f255fb0', '2020-11-23 15:39:50', 'marto22@gmail.com'),
	(32, '38ffc26a-2589-4f01-80a2-3447af17107a', '2020-11-23 15:41:14', 'marto@abv.bg'),
	(33, 'f783b2cf-b3e3-4a65-ae48-8e08ca088bdd', '2020-11-23 18:18:13', 'YzMPKrIgXZ@gmail.com'),
	(34, 'b9ab436a-beb9-492b-a429-dfcf25365de4', '2020-11-23 18:22:56', 'uFJiXukDPB@gmail.com'),
	(35, '78416293-221b-4065-9ed5-c0d556d719a8', '2020-11-23 18:25:04', 'vwHMvSpcbp@gmail.com'),
	(36, 'b951af52-498b-4afe-8c54-e37f5757b673', '2020-11-23 21:53:52', 'evaHF@gmail.com'),
	(37, '8b811c91-12fe-4c07-a9ca-16db8219c83a', '2020-11-23 21:54:40', 'XWjKp@gmail.com'),
	(38, 'da6abf00-bec7-4f42-9f7d-03e10eef8894', '2020-11-23 21:55:15', 'ZxFam@gmail.com'),
	(39, '1345b2a7-84c3-437e-892a-9a510ef92fd3', '2020-11-23 21:56:36', 'PkkXd@gmail.com'),
	(40, '61ece3c7-664c-4b43-bfa3-c6654abef435', '2020-11-23 22:42:18', 'fyAMmBmyjh@gmail.com'),
	(41, '9ed639ec-11f8-4515-b4f0-550e93094f17', '2020-11-23 23:00:48', 'NPwaOvqnCg@gmail.com'),
	(42, '09b8c813-e34e-4a04-99e8-63170a4d4caf', '2020-11-23 23:08:44', 'hsgMMLWAxY@gmail.com'),
	(43, '7b75d60f-9bba-4272-b289-00103f7e9621', '2020-11-23 23:11:51', 'YEMAiljUOO@gmail.com'),
	(44, '19889408-3d87-4fa5-9734-66b88fa4129a', '2020-11-23 23:13:20', 'mjOvHbncFm@gmail.com'),
	(45, '37c75fd3-7305-43ce-aea9-7cb61be3075b', '2020-11-23 23:13:44', 'ArjsKQxvvh@gmail.com'),
	(46, '352dd407-4efb-485b-be84-1d56e8773969', '2020-11-23 23:39:08', 'marto221@gmail.com'),
	(47, '1d260ab0-b70a-4b43-87df-f13246a96a32', '2020-11-24 00:10:08', 'LGiNV@gmail.com'),
	(48, 'dead5f6e-d759-4c4a-b0d5-ffaf8b23ab2d', '2020-11-24 15:57:03', 'test1234@gmail.com'),
	(49, '4f1614df-f500-41dc-83b1-53cb83d6e3c0', '2020-11-24 18:46:00', 'renbSpQMCV@gmail.com'),
	(50, '7d9dc9ae-0c30-4497-ab9d-0a08c3bdb194', '2020-11-24 19:48:44', 'sUFFkfpNIB@gmail.com'),
	(51, '23da6b2c-f543-4e0a-87b6-0852bf279e13', '2020-11-24 19:49:10', 'CZtxipdhBC@gmail.com'),
	(52, 'eddac38d-06bd-4029-80dc-f684ab6a2431', '2020-11-24 19:50:31', 'AbXFJMJDVE@gmail.com'),
	(53, '7ada5384-e2ed-40d6-b908-32095cc3ffb0', '2020-11-24 22:57:20', 'EASHi@gmail.com'),
	(54, '8200fe93-23f1-419c-8bb5-01ba712b3ac0', '2020-11-24 23:10:41', 'ZeXet@gmail.com'),
	(55, '7cadd892-c5bd-46d0-93a2-4e79cbf5d6aa', '2020-11-25 01:50:52', 'hFPwn@gmail.com'),
	(56, '7af68225-319c-4148-bb49-9b84185d3904', '2020-11-25 15:49:12', 'ztSDA@gmail.com'),
	(57, '760c6935-8eea-4765-84bd-6faca31b0102', '2020-11-25 21:02:27', 'kZqYx@gmail.com'),
	(58, '399770c5-e4af-4d00-9aef-9859d8b799ef', '2020-11-25 21:42:13', 'oeweL@gmail.com'),
	(59, '3719b869-c3e6-4d53-be41-9949791ba5bd', '2020-11-25 22:54:24', 'RQwxu@gmail.com'),
	(60, '8a1bf534-3a02-4e08-a04a-08478e356f10', '2020-11-25 22:55:02', 'LEfTZ@gmail.com'),
	(61, '9ad0e236-41ab-4e1d-ba90-d0b54ad5ac34', '2020-11-25 22:56:01', 'nluVw@gmail.com'),
	(62, '3dcb8c37-87ee-447f-9add-1941c10520eb', '2020-11-26 01:29:54', 'bRcPe@gmail.com'),
	(63, '38c0e8e2-99ac-4123-8ce4-c77b53b72329', '2020-11-26 01:31:25', 'BvXtj@gmail.com'),
	(64, 'ccc41a6f-a473-4929-82d2-d0908d1f4e63', '2020-11-26 02:10:18', 'JXYcQ@gmail.com'),
	(65, '420cd064-b855-4cc0-b42c-69927cbdb12c', '2020-11-26 02:11:52', 'bpQXz@gmail.com'),
	(66, '169d2d02-4383-46f2-910b-d1f0d0b54731', '2020-11-26 02:15:55', 'nAKPV@gmail.com'),
	(67, '4b11169f-acf6-4ec1-9fa4-f5dd4260b5e7', '2020-11-26 02:18:52', 'xaKyM@gmail.com'),
	(68, 'b0cfe1b8-4db7-4080-a886-26a589802501', '2020-11-26 09:44:46', 'LUCVS@gmail.com'),
	(69, 'e99b9431-d054-477b-abdf-ee877dc64213', '2020-11-26 11:17:56', 'DlOYi@gmail.com'),
	(70, 'd0a8409d-c7e0-4c85-b467-40a23fd4787b', '2020-11-26 11:22:59', 'LopPL@gmail.com'),
	(71, '2db1e406-889c-4b6b-b6df-55ac01527c88', '2020-11-26 12:17:07', 'NWpDv@gmail.com'),
	(72, '12723050-6cee-4ff5-8a36-f7a7ff73eddc', '2020-11-26 12:21:07', 'WuLgk@gmail.com'),
	(73, '90a8ac22-b14d-4059-bf47-15591c837cf1', '2020-11-26 12:26:33', 'KNtYd@gmail.com'),
	(74, 'ac248ecd-2c3e-4b4f-8cf7-eee67a0a4775', '2020-11-26 12:35:08', 'egcaF@gmail.com'),
	(75, 'f922c620-053c-42d3-a83d-799a26bbef87', '2020-11-26 12:59:33', 'qTkUK@gmail.com'),
	(76, 'a461cab6-d5c8-4940-9447-03e5441d38cb', '2020-11-27 19:42:46', 'deleted_user@gmail.com'),
	(77, '9e9668f2-0fe7-4868-ab75-2b6ce22409dc', '2020-11-27 23:25:50', 'tpOXs@gmail.com'),
	(78, 'dd41d839-a802-4799-af59-eabddad2b12d', '2020-11-28 23:25:06', 'UbyKN@gmail.com'),
	(79, 'f05e1f09-e986-4e28-ac6b-bdb7ef055f30', '2020-11-28 23:29:01', 'LQAkW@gmail.com'),
	(80, '34dbcf5e-bb41-454d-b51b-918f5e512e8b', '2020-11-28 23:56:29', 'KHCXP@gmail.com');
/*!40000 ALTER TABLE `confirmation_tokens` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.connections
CREATE TABLE IF NOT EXISTS `connections` (
  `connection_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sender_id` bigint(20) NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`connection_id`),
  UNIQUE KEY `connections_pk_2` (`sender_id`,`receiver_id`),
  KEY `FKnpqlxjk46fepdhmfcskdpe15o` (`receiver_id`),
  KEY `FKjbvv2hkxns7igsslu3i5u80q0` (`status_id`),
  CONSTRAINT `FKcq4mvidi7nlcg14ynpkibid0b` FOREIGN KEY (`sender_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `FKjbvv2hkxns7igsslu3i5u80q0` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`),
  CONSTRAINT `FKnpqlxjk46fepdhmfcskdpe15o` FOREIGN KEY (`receiver_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `connections_statuses_status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`),
  CONSTRAINT `connections_users_details_user_details_id_fk` FOREIGN KEY (`sender_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `connections_users_details_user_details_id_fk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users_details` (`user_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.connections: ~5 rows (approximately)
DELETE FROM `connections`;
/*!40000 ALTER TABLE `connections` DISABLE KEYS */;
INSERT INTO `connections` (`connection_id`, `sender_id`, `receiver_id`, `status_id`) VALUES
	(140, 3, 1, 1),
	(144, 5, 4, 1),
	(156, 11, 13, 2),
	(157, 3, 2, 1),
	(158, 2, 3, 1);
/*!40000 ALTER TABLE `connections` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.gender
CREATE TABLE IF NOT EXISTS `gender` (
  `gender_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`gender_id`),
  UNIQUE KEY `gender_type_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.gender: ~2 rows (approximately)
DELETE FROM `gender`;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` (`gender_id`, `type`, `enabled`) VALUES
	(1, 'male', 1),
	(2, 'female', 1);
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.media
CREATE TABLE IF NOT EXISTS `media` (
  `media_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `visibility_id` int(11) NOT NULL,
  `path` varchar(200) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`media_id`),
  KEY `FKqlmgx5ek82duyswj7c6vn3i33` (`type_id`),
  KEY `FK8lotl0mc86mtscx49gx0dddt3` (`visibility_id`),
  CONSTRAINT `FK8lotl0mc86mtscx49gx0dddt3` FOREIGN KEY (`visibility_id`) REFERENCES `visibilities` (`visibility_id`),
  CONSTRAINT `FKqlmgx5ek82duyswj7c6vn3i33` FOREIGN KEY (`type_id`) REFERENCES `media_types` (`type_id`),
  CONSTRAINT `media_media_types_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `media_types` (`type_id`),
  CONSTRAINT `user_profile_pictures_visabilities_visability_id_fk` FOREIGN KEY (`visibility_id`) REFERENCES `visibilities` (`visibility_id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.media: ~237 rows (approximately)
DELETE FROM `media`;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` (`media_id`, `type_id`, `visibility_id`, `path`, `enabled`) VALUES
	(1, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606338702/qjrpmdeqsvc9xxqebvfd.jpg', 1),
	(2, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605525483/vndwa9shvw2cjwi9eeyr.jpg', 1),
	(3, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603718553/qtdvmalnwcd8lbqadqcj.jpg', 1),
	(4, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603719195/zxsf3b6pu40z0cvxyzhx.jpg', 1),
	(5, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606206624/awhcvnq39drbb4kqx4oc.jpg', 1),
	(6, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603805846/bo2pzixbcngmwvtzrsmt.jpg', 0),
	(7, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603810879/op9fockpvgrl446kisak.jpg', 0),
	(8, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603811005/m06lsbvysraxhpd9tros.jpg', 0),
	(9, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603811810/lugxgwjlirneojkkbvtx.jpg', 1),
	(10, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1603817314/vxgw4scbnatpkjt9lfwu.jpg', 0),
	(11, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1604313252/hzy1705sd5laldwbqzw5.jpg', 1),
	(12, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1604401669/rswse1agysb3rtybfp6u.jpg', 1),
	(13, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1604911277/l9olizcelhjqlafh0wno.jpg', 0),
	(14, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1604912747/f6osiarjs4wpdrezmuxg.jpg', 1),
	(15, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605024909/cbvyydgsuzmklso8nksl.jpg', 1),
	(16, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605025924/z45enrl3gn3vvojr22ga.jpg', 1),
	(17, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605026279/lv4uhcbotdlqlpnwdoe8.jpg', 1),
	(18, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605028187/psqutptdle8la2xq6r5x.jpg', 1),
	(19, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605041413/eaugqxzq5fadtmdhvay7.jpg', 1),
	(20, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605044394/qd3wwjiryvdyexfk2lya.png', 1),
	(21, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605048219/tstfrdeeo3vex5dk64ht.png', 1),
	(22, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605049987/yeuprgicrmbw2xrph4vk.png', 1),
	(23, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605089680/sgu9v0kway3dry9oey3a.png', 1),
	(24, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605091276/dde1m9lgoupddmgzevpz.mp4', 1),
	(25, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605168791/mnbxuegko8i9kesukiam.png', 1),
	(26, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605168918/toxhh6upggeyzzsow8nu.png', 1),
	(27, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605169381/ppb4ie9sw6p9wdfgshnw.png', 1),
	(28, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605169776/oxihnzkriwy4ai4bq1sy.png', 1),
	(29, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605172122/flwxq8dbwzdclj0ccxfq.png', 1),
	(30, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605394632/c9mw49qfzyk7psojlvqx.mp4', 0),
	(31, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605395940/mm6uooqbdsgaeuubm0gb.mp4', 0),
	(32, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605396894/lttbjenakzh607re6bzf.png', 0),
	(33, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605616339/avtvrmn9t4kvtxlujsvd.mp4', 0),
	(34, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605616475/pgwjnk9jqehkysletlr6.mp4', 0),
	(35, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605616519/hnlarewubbxgwfuiiesy.mp4', 0),
	(36, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605617105/acohqusyv0afprkofwxp.mov', 0),
	(37, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605617211/jyzaterbottm7ccedheg.mp4', 0),
	(38, 2, 1, 'http://res.cloudinary.com/finalprojectnarnia/video/upload/v1605617362/nzbqcyx14cvrqhmbytrr.webm', 0),
	(39, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605780291/cr4fkirqdopuyeqeith6.png', 0),
	(40, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605783177/agsflcz1nr1bs26o4z7m.jpg', 0),
	(41, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605784820/jqzfojwjyklddffro5nb.png', 0),
	(42, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605785065/f96wamukky3yzwsswyyw.jpg', 0),
	(43, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605785195/zcraxopv74ryfu5znkzp.jpg', 0),
	(44, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605785642/yd9yeyph0nuy178pkdgq.jpg', 0),
	(45, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605786642/uks7grxd8xhmulw33sdx.jpg', 0),
	(46, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605786783/roemy7eryserrtwltepy.jpg', 0),
	(47, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605787410/hnsyytszowen1vfybv1c.jpg', 0),
	(48, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605788362/ztxudumnwu36r4gksouz.jpg', 0),
	(49, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605788651/wvc4ytbpkbfbuviqibzl.jpg', 0),
	(50, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605791483/xmqt9juwmatmnwbmdvtu.jpg', 0),
	(51, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605792318/yljsyrbo0dfxf3fk1uro.jpg', 0),
	(52, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605792479/zvnnv1xznfcoaunin7qv.jpg', 0),
	(53, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605793862/ldldk4r2vl4k9gb3dezn.jpg', 0),
	(54, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605794757/lftl6nedwxpk9guarjts.jpg', 0),
	(55, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605804365/upml0azrzmrvw7sabhkj.jpg', 0),
	(56, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605822663/l783zgzvxdgchx1jvoti.png', 1),
	(57, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605822763/mpqq6rhdtef1po6z3l0b.jpg', 1),
	(58, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605823215/vmi2serpkgkzqeyzbw1h.jpg', 1),
	(59, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605823358/rm9rynvw8kcu7p4fow3f.jpg', 0),
	(60, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605823552/wb9ka4kb9gpxflraag3k.jpg', 0),
	(61, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605824080/g3yzxn6pqkybrg8qslm9.jpg', 0),
	(62, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605832281/tpjqoud01sysaaxcfzka.jpg', 0),
	(63, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605859755/j8rjx7eu1o0c4iu9fxsu.jpg', 0),
	(64, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605860356/yejunuzmtnmdug0nnpt5.jpg', 0),
	(65, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605906802/zlsnilliktia9c2bjpzk.jpg', 0),
	(66, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605957984/whpzy1wkrjjaopz8tfip.jpg', 0),
	(67, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605958231/k9uca8dvwotn3jexnvvc.jpg', 0),
	(68, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605958362/mvpgvnpggmqznstkobfm.jpg', 0),
	(69, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605959465/ik4otxhzywrejgawfnov.jpg', 0),
	(70, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605959567/tjxfixaoy60svx2csjvi.jpg', 0),
	(71, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605960241/uchmlsqm5lridnru4isi.jpg', 0),
	(72, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605960779/bswxj5yn5zgs8wfghfcz.jpg', 0),
	(73, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605987938/ys2jjbhzl43sf3ftbvp5.jpg', 0),
	(74, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605992901/sszy78f5ghrogzdt0yvy.jpg', 0),
	(75, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605993090/z3wr1puvmtxph2j1fh6d.jpg', 0),
	(76, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605993300/ikxvvt9jispvlmmkvomo.jpg', 0),
	(77, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605993405/b2wfke4tmxxrgwqxmksn.jpg', 0),
	(78, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605993892/qj3dxzxekhzgmq1xzqxv.jpg', 0),
	(79, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994066/blrze49q1cib026lcafc.jpg', 0),
	(80, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994310/ryrbglfuvfwox5dx4jzs.jpg', 0),
	(81, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994437/jutatc0y7sgp9eztktr6.jpg', 0),
	(82, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994613/tzm0dsoulht1ujjplnib.jpg', 0),
	(83, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605994767/s9p5bs6r6b4oqnq1ygkc.jpg', 0),
	(84, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605995233/lrvtshvhirp3hbfdpa7y.jpg', 0),
	(85, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605995323/pgwbcqnpzsfijjmrf7n1.jpg', 0),
	(86, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605995475/dppadwijn3iwcqapfd8h.jpg', 0),
	(87, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605996015/pb8cncv9yv5doa3sxjht.jpg', 0),
	(88, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605996363/wr1kdlc24tkmhc67iybl.jpg', 0),
	(89, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605999112/bygwk7s735wb3w5iklpl.jpg', 0),
	(90, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605999350/kpzasu5xi8xzztkot2nb.jpg', 0),
	(91, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605999805/ykkburexdbagxopylo1f.jpg', 0),
	(92, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1605999892/r0svkk71ar9boiy3xxkn.jpg', 0),
	(93, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606000737/zo7ygqmnet4o1zv180b3.jpg', 0),
	(94, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606035782/r0u8jrxz1jwzaftablos.jpg', 0),
	(95, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606035972/tlx4tpqba35jm1n0z5lc.jpg', 0),
	(96, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606036920/hgat6l7rypqqdj4ssahn.jpg', 0),
	(97, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606039107/bdqwidqovpltwhd6uo1s.jpg', 0),
	(98, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606039448/u15guomh93kzbjfvoreh.jpg', 0),
	(99, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606039839/obfgykl6v7wdjyphpqqn.jpg', 0),
	(100, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606040053/lp1kw1jmmpaomxikdyrp.jpg', 0),
	(101, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606040415/jrx8injya7bfnojor1b0.jpg', 0),
	(102, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606040840/x9e5bamgfzrsqrrl7l0a.jpg', 0),
	(103, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606041044/ngirp0k8ls6fipryfone.jpg', 0),
	(104, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606049178/xjxyrad0qhatkbxkpama.jpg', 0),
	(105, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606049379/miepjsyyshaqdm2wjrve.jpg', 0),
	(106, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606081660/bj7lttiutyvnvrpvnhjy.jpg', 0),
	(107, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606081744/c3vq777brkbew0ecjtfs.jpg', 0),
	(108, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606082019/dywslbni4fsgsad0spm3.jpg', 0),
	(109, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606083352/lzc0x1xwnx5vq3vsw2va.jpg', 0),
	(110, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606083951/fi7wmcdb6xhv8tqfzwn4.jpg', 0),
	(111, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606084088/y85yq1vwvdktecdzkdpy.jpg', 0),
	(112, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606084949/iesgpjjpk8sdvbis9fsy.jpg', 0),
	(113, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606087578/iuxbvfyeu5tqniq91ufl.jpg', 0),
	(114, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606088244/oyxbujn8w33g96klnqz3.jpg', 0),
	(115, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606088885/iwwbcky4juw3j1czoqqf.jpg', 0),
	(116, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606089080/zuavtxafpjurjnszeyqs.jpg', 0),
	(117, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606089117/ui35vf9rqcg5whrl7kav.jpg', 0),
	(118, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606089396/oxsd9gqdv4cdos3ywv9f.jpg', 0),
	(119, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606089859/fphhyi8wnt5psn03gctu.jpg', 0),
	(120, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606090066/yo6p1ur4dz2dtewfpkgo.jpg', 0),
	(121, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606090272/fjagax54au71c3iqxpdr.jpg', 0),
	(122, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606090457/y3oiajo9mrszuvgkacae.jpg', 0),
	(123, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606090675/mfillehght7zzlgbbobg.jpg', 0),
	(124, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606093425/xzgccot5eagaizyhyng3.png', 1),
	(125, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606093465/rif06eqs8iw9rud5z9l8.png', 1),
	(126, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606117281/zodyzt2sh18fx1izyac0.jpg', 0),
	(127, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606123034/dfgiemzlbiet1j1pimji.jpg', 0),
	(128, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606123194/sskija5ghg5uk9xraoaz.jpg', 0),
	(129, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606123537/nsupwrexzaomjou8eszk.jpg', 0),
	(130, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606123548/ehpkpe1ewtldbj9qsq4c.jpg', 0),
	(131, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606124010/tavuwcx1s4uxmttvgq8t.jpg', 0),
	(132, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606124307/hxpy08h7lfozyie8mqup.jpg', 0),
	(133, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606128281/nwhbxejewv4aqlg4ady4.jpg', 1),
	(134, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606133454/fbxijwaduakdgdt5qfss.jpg', 1),
	(135, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606138789/qxehhpfrey2whonjdywl.png', 1),
	(136, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606138873/af95vz5b6vzvblodhjb0.png', 1),
	(137, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606148292/u2tlkqn9nmrxqvxfhi5q.jpg', 1),
	(138, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606148575/ihhvua3y6nv3vughgodp.jpg', 1),
	(139, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606148702/fatd5c7jthkjgs8cioud.jpg', 1),
	(140, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606161231/x1mv9siezjp5syagaeg1.png', 1),
	(141, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606161278/iysnwzsw8wz6drnvnb7q.png', 1),
	(142, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606161314/emq1jg8uahnviumdlfau.png', 1),
	(143, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606161394/evk9p1z5awmot2tkmyfg.png', 1),
	(144, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606164137/evoykagnh42tkpp9hslc.jpg', 1),
	(145, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606164155/qca8fmm5kmr09xhttl7j.jpg', 0),
	(146, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606164684/qbkmfbescwrvbzebusa3.jpg', 0),
	(147, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606164931/le2fl6i8gj9hrgvq7tnl.jpg', 0),
	(148, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165096/ygdqrpalyuopoaalbive.jpg', 0),
	(149, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165246/oxrbqqcvxsa9s82h5dxc.jpg', 1),
	(150, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165533/qnqsme0jgsjghnd6txli.jpg', 0),
	(151, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165722/hrqknilgavcxu6m9cj16.jpg', 1),
	(152, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165909/cughcvsgh6khd9yzqhjs.jpg', 1),
	(153, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606165998/xqmo8nhlwjonxdwpyllc.jpg', 1),
	(154, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606166021/plfymsh5ftlgjvrqfbm1.jpg', 1),
	(155, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606167547/yn8swks7ri5gypxlyxvp.png', 1),
	(156, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606168425/s0v7snfpk4hdbelrsnsi.jpg', 0),
	(157, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606169205/gylyosathwjwjnrq2erl.jpg', 0),
	(158, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606169407/sun9i8bzz0jqzisocodo.png', 1),
	(159, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606169481/zqh9xm9n3eleirsmedb7.jpg', 0),
	(160, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606226220/vv6zj2ywwkbomzk9pidj.png', 1),
	(161, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606236358/v40uinfc6vwmkmri73nd.jpg', 1),
	(162, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606240122/fsmobxpuyczykktkupu6.jpg', 1),
	(163, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606240149/tnpfxsvgkpcjexb3wftf.jpg', 1),
	(164, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606240229/vhft3lxt8m54noh3j01w.jpg', 1),
	(165, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606251438/z015crozn8bhhp1ukgcg.png', 1),
	(166, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606252240/btlbpd5uhh5hfgnygjs2.png', 1),
	(167, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606261851/clxeotiwo926wvvjtnfn.png', 1),
	(168, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606312150/q36v1hfeskctyxirkyul.png', 1),
	(169, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606312667/tzdbnwdhlwzcapzlc1se.jpg', 0),
	(170, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606330945/b5kuphiphad7l7ku1ubf.png', 1),
	(171, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606333331/hzjzdy75s8voucekzp7q.png', 1),
	(172, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606337663/f1cea6mzgsltbobyfusl.png', 1),
	(173, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606337701/eetlmbuyxv5roekcp0js.png', 1),
	(174, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606337760/ptdgjglk84ctgglbrq2c.png', 1),
	(175, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606346992/nar6civnfq4damjvjzjx.png', 1),
	(176, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606347083/wh3u4ncjhft6wozgu0wf.png', 1),
	(177, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606349416/jqdgil1idyczkjnk3sl9.png', 1),
	(178, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606349510/rtlzxvvehdhktr2xboip.png', 1),
	(179, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606349753/rcv2jdr2vy0m3uajvkng.png', 1),
	(180, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606349931/ve8yjnyxze0jmg5vpjhw.png', 1),
	(181, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606349964/jxguy5rxzbro2tqc8l6h.jpg', 0),
	(182, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606376685/lbs9pld4drem9r6yqlos.png', 1),
	(183, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606382274/s4y23zkobpytfw3rhnzt.png', 1),
	(184, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606382578/xwmxpjn6ynmbwlqstx0l.png', 1),
	(185, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606385824/uyvf85vvlkmg4d2jsxok.png', 1),
	(186, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606386066/rd5zjsgea0gfawincmfc.png', 1),
	(187, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606386391/wpmwvskvm5k23hvgx2yy.png', 1),
	(188, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606386906/e2b4dzuzxofyekcyrtol.png', 1),
	(189, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606388372/rt5a8ued9keyzachamkh.png', 1),
	(190, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606465139/kastpibcxkgxhd6w94ox.png', 0),
	(191, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606475447/kezjpyldtx48rfxwz2fg.jpg', 0),
	(192, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606477294/rorrymoycxs2zcub5urw.jpg', 0),
	(193, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606478261/gesjhgrzsmixcqkpxytm.jpg', 0),
	(194, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606483249/ualhfzejyw0hytmwhfco.jpg', 0),
	(195, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606484876/qimshpl3q3w2jo0b6kxi.jpg', 0),
	(196, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606485356/i4aotn2vwxv7crdcqvf4.jpg', 0),
	(197, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606485454/uri35x3v53jqilqetjym.jpg', 0),
	(198, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606485535/xtyy4gdlfcpjxtj0kyo3.jpg', 0),
	(199, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490554/sux0zbttkcx4qld5wbqd.jpg', 0),
	(200, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490556/a2qajlgmh8iqgy3dgofl.jpg', 0),
	(201, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490632/rsw0beeffpigl9gyeu5y.jpg', 0),
	(202, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606490633/vga3vzoo80wxme12xsxc.jpg', 0),
	(203, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606498561/vlsiaedfevvxvhmpre7o.jpg', 0),
	(204, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606498563/fqc1atff3wlo9tkmh3ck.jpg', 0),
	(205, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606498967/s545onysuo7y010dncfa.jpg', 1),
	(206, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499036/ufqpu1wsoihbct2qpeig.jpg', 0),
	(207, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499038/hmb6ntafvpvdvkrny9yc.jpg', 0),
	(208, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499115/oyvut4d3nwedbolsrahm.jpg', 0),
	(209, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499117/gjtuvxmxk8zxr6bzr6dz.jpg', 0),
	(210, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499194/tzfrelpjerx2y3ndtjvu.jpg', 0),
	(211, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606499195/nujwbotjj5xhn3uorp2j.jpg', 0),
	(212, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503260/bd4bpgb07jca4t855bzt.jpg', 0),
	(213, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503263/bwxc0figmucy6draz8b1.jpg', 0),
	(214, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503344/ltta64ghnxrznyfdz666.jpg', 0),
	(215, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503346/pl1gfcpkde8mhkd3zmiw.jpg', 0),
	(216, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503482/pqlfewegewvdapnqr5th.jpg', 0),
	(217, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503484/r3uai2sw8h5kdifxgtri.jpg', 0),
	(218, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503562/qzaw94m75g6is7d04ehj.jpg', 0),
	(219, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503565/yaz5yns65o7vw2xflctf.jpg', 0),
	(220, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503671/cyaztr4ptroajn468eta.jpg', 0),
	(221, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503674/jjvt4k6eu5r7snrz5wse.jpg', 0),
	(222, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503995/ubseba9mfcttpn3ainke.jpg', 0),
	(223, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606503998/ywa6b4qr5l43efuxfyoq.jpg', 0),
	(224, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606505405/us0cjvfr1fswalyjdxxb.jpg', 0),
	(225, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606505407/whcmvwwprygm0sndfqb9.jpg', 0),
	(226, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606512349/a6u0wik6dbqmmtzpzdpq.png', 1),
	(227, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606577919/k8qjenlom7vwv8oyemd5.jpg', 0),
	(228, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606577921/wi6sa1fbnyprbjkkzz45.jpg', 0),
	(229, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606590488/fdmzxonojufvdmhg2dks.jpg', 0),
	(230, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606590490/jljeddptpqikpt8dskxw.jpg', 0),
	(231, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606596485/ghjlljz1ytenwdxi989v.jpg', 0),
	(232, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606596487/dv76fwzxxjr0nvlf83mk.jpg', 0),
	(233, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606598704/a1k1futltmjsegrp5qvy.png', 1),
	(234, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606598939/jjdbkiilqpzxilq2zyit.png', 1),
	(235, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606598975/k8tiq9xstxnpqlczspx0.jpg', 0),
	(236, 1, 1, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606600587/cxyg82qgblrc4hr0tk8q.png', 1),
	(237, 1, 2, 'http://res.cloudinary.com/finalprojectnarnia/image/upload/v1606600626/pe7ivewr74meqj7yspqj.jpg', 0);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.media_types
CREATE TABLE IF NOT EXISTS `media_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `media_types_name_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.media_types: ~2 rows (approximately)
DELETE FROM `media_types`;
/*!40000 ALTER TABLE `media_types` DISABLE KEYS */;
INSERT INTO `media_types` (`type_id`, `type`, `enabled`) VALUES
	(1, 'image', 1),
	(2, 'video', 1);
/*!40000 ALTER TABLE `media_types` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.nationalities
CREATE TABLE IF NOT EXISTS `nationalities` (
  `nationality_id` int(11) NOT NULL AUTO_INCREMENT,
  `nationality` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`nationality_id`),
  UNIQUE KEY `nationalities_nationality_uindex` (`nationality`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.nationalities: ~252 rows (approximately)
DELETE FROM `nationalities`;
/*!40000 ALTER TABLE `nationalities` DISABLE KEYS */;
INSERT INTO `nationalities` (`nationality_id`, `nationality`, `enabled`) VALUES
	(1, 'Afghan', 1),
	(2, 'Albanian', 1),
	(3, 'Algerian', 1),
	(4, 'American', 1),
	(5, 'Andorran', 1),
	(6, 'Angolan', 1),
	(7, 'Antiguans', 1),
	(8, 'Argentinean', 1),
	(9, 'Armenian', 1),
	(10, 'Australian', 1),
	(11, 'Austrian', 1),
	(12, 'Azerbaijani', 1),
	(13, 'Bahamian', 1),
	(14, 'Bahraini', 1),
	(15, 'Bangladeshi', 1),
	(16, 'Barbadian', 1),
	(17, 'Barbudans', 1),
	(18, 'Batswana', 1),
	(19, 'Belarusian', 1),
	(20, 'Belgian', 1),
	(21, 'Belizean', 1),
	(22, 'Beninese', 1),
	(23, 'Bhutanese', 1),
	(24, 'Bolivian', 1),
	(25, 'Bosnian', 1),
	(26, 'Brazilian', 1),
	(27, 'British', 1),
	(28, 'Bruneian', 1),
	(29, 'Bulgarian', 1),
	(30, 'Burkinabe', 1),
	(31, 'Burmese', 1),
	(32, 'Burundian', 1),
	(33, 'Cambodian', 1),
	(34, 'Cameroonian', 1),
	(35, 'Canadian', 1),
	(36, 'Cape Verdean', 1),
	(37, 'Central African', 1),
	(38, 'Chadian', 1),
	(39, 'Chilean', 1),
	(40, 'Chinese', 1),
	(41, 'Colombian', 1),
	(42, 'Comoran', 1),
	(43, 'Congolese', 1),
	(44, 'Costa Rican', 1),
	(45, 'Croatian', 1),
	(46, 'Cuban', 1),
	(47, 'Cypriot', 1),
	(48, 'Czech', 1),
	(49, 'Danish', 1),
	(50, 'Djibouti', 1),
	(51, 'Dominican', 1),
	(52, 'Dutch', 1),
	(53, 'East Timorese', 1),
	(54, 'Ecuadorean', 1),
	(55, 'Egyptian', 1),
	(56, 'Emirian', 1),
	(57, 'Equatorial Guinean', 1),
	(58, 'Eritrean', 1),
	(59, 'Estonian', 1),
	(60, 'Ethiopian', 1),
	(61, 'Fijian', 1),
	(62, 'Filipino', 1),
	(63, 'Finnish', 1),
	(64, 'French', 1),
	(65, 'Gabonese', 1),
	(66, 'Gambian', 1),
	(67, 'Georgian', 1),
	(68, 'German', 1),
	(69, 'Ghanaian', 1),
	(70, 'Greek', 1),
	(71, 'Grenadian', 1),
	(72, 'Guatemalan', 1),
	(73, 'Guinea-Bissauan', 1),
	(74, 'Guinean', 1),
	(75, 'Guyanese', 1),
	(76, 'Haitian', 1),
	(77, 'Herzegovinian', 1),
	(78, 'Honduran', 1),
	(79, 'Hungarian', 1),
	(80, 'I-Kiribati', 1),
	(81, 'Icelander', 1),
	(82, 'Indian', 1),
	(83, 'Indonesian', 1),
	(84, 'Iranian', 1),
	(85, 'Iraqi', 1),
	(86, 'Irish', 1),
	(87, 'Israeli', 1),
	(88, 'Italian', 1),
	(89, 'Ivorian', 1),
	(90, 'Jamaican', 1),
	(91, 'Japanese', 1),
	(92, 'Jordanian', 1),
	(93, 'Kazakhstani', 1),
	(94, 'Kenyan', 1),
	(95, 'Kittian and Nevisian', 1),
	(96, 'Kuwaiti', 1),
	(97, 'Kyrgyz', 1),
	(98, 'Laotian', 1),
	(99, 'Latvian', 1),
	(100, 'Lebanese', 1),
	(101, 'Liberian', 1),
	(102, 'Libyan', 1),
	(103, 'Liechtensteiner', 1),
	(104, 'Lithuanian', 1),
	(105, 'Luxembourger', 1),
	(106, 'Macedonian', 1),
	(107, 'Malagasy', 1),
	(108, 'Malawian', 1),
	(109, 'Malaysian', 1),
	(110, 'Maldivian', 1),
	(111, 'Malian', 1),
	(112, 'Maltese', 1),
	(113, 'Marshallese', 1),
	(114, 'Mauritanian', 1),
	(115, 'Mauritian', 1),
	(116, 'Mexican', 1),
	(117, 'Micronesian', 1),
	(118, 'Moldovan', 1),
	(119, 'Monacan', 1),
	(120, 'Mongolian', 1),
	(121, 'Moroccan', 1),
	(122, 'Mosotho', 1),
	(123, 'Motswana', 1),
	(124, 'Mozambican', 1),
	(125, 'Namibian', 1),
	(126, 'Nauruan', 1),
	(127, 'Nepalese', 1),
	(128, 'New Zealander', 1),
	(129, 'Ni-Vanuatu', 1),
	(130, 'Nicaraguan', 1),
	(131, 'Nigerian', 1),
	(132, 'Nigerien', 1),
	(133, 'North Korean', 1),
	(134, 'Northern Irish', 1),
	(135, 'Norwegian', 1),
	(136, 'Omani', 1),
	(137, 'Pakistani', 1),
	(138, 'Palauan', 1),
	(139, 'Panamanian', 1),
	(140, 'Papua New Guinean', 1),
	(141, 'Paraguayan', 1),
	(142, 'Peruvian', 1),
	(143, 'Polish', 1),
	(144, 'Portuguese', 1),
	(145, 'Qatari', 1),
	(146, 'Romanian', 1),
	(147, 'Russian', 1),
	(148, 'Rwandan', 1),
	(149, 'Saint Lucian', 1),
	(150, 'Salvadoran', 1),
	(151, 'Samoan', 1),
	(152, 'San Marinese', 1),
	(153, 'Sao Tomean', 1),
	(154, 'Saudi', 1),
	(155, 'Scottish', 1),
	(156, 'Senegalese', 1),
	(157, 'Serbian', 1),
	(158, 'Seychellois', 1),
	(159, 'Sierra Leonean', 1),
	(160, 'Singaporean', 1),
	(161, 'Slovakian', 1),
	(162, 'Slovenian', 1),
	(163, 'Solomon Islander', 1),
	(164, 'Somali', 1),
	(165, 'South African', 1),
	(166, 'South Korean', 1),
	(167, 'Spanish', 1),
	(168, 'Sri Lankan', 1),
	(169, 'Sudanese', 1),
	(170, 'Surinamer', 1),
	(171, 'Swazi', 1),
	(172, 'Swedish', 1),
	(173, 'Swiss', 1),
	(174, 'Syrian', 1),
	(175, 'Taiwanese', 1),
	(176, 'Tajik', 1),
	(177, 'Tanzanian', 1),
	(178, 'Thai', 1),
	(179, 'Togolese', 1),
	(180, 'Tongan', 1),
	(181, 'Trinidadian or Tobagonian', 1),
	(182, 'Tunisian', 1),
	(183, 'Turkish', 1),
	(184, 'Tuvaluan', 1),
	(185, 'Ugandan', 1),
	(186, 'Ukrainian', 1),
	(187, 'Uruguayan', 1),
	(188, 'Uzbekistani', 1),
	(189, 'Venezuelan', 1),
	(190, 'Vietnamese', 1),
	(191, 'Welsh', 1),
	(192, 'Yemenite', 1),
	(193, 'Zambian', 1),
	(194, 'Zimbabwean', 1),
	(195, 'Aya', 0),
	(196, 'Babys', 0),
	(197, '12', 0),
	(198, 'Beyonc ha ha ha ha ha ha ha hahaaaaaaa  ha ha haha', 0),
	(199, 'BG', 1),
	(200, 'New Nationality', 0),
	(201, '<string>', 0),
	(202, 'Est minus officiis omnis.', 0),
	(203, 'et', 0),
	(204, 'quod', 0),
	(205, 'quo', 0),
	(206, 'saepe', 0),
	(207, 'similique', 0),
	(208, 'eaque', 0),
	(209, 'in', 0),
	(210, 'quos', 0),
	(211, 'ut', 0),
	(212, 'sit', 0),
	(213, 'odit', 0),
	(214, 'qui', 0),
	(215, 'doloribus', 0),
	(217, 'est', 0),
	(219, 'atque', 0),
	(220, 'cupiditate', 0),
	(222, 'voluptas', 0),
	(223, 'distinctio', 0),
	(224, 'temporibus', 0),
	(225, 'facilis', 0),
	(226, 'occaecati', 0),
	(227, 'aut', 0),
	(228, 'voluptatem', 0),
	(231, 'cum', 0),
	(232, 'ullam', 0),
	(233, 'odio', 0),
	(234, 'magnam', 0),
	(238, '7e8d514d-20c7-4e09-97dd-9f524a76cd3a', 0),
	(239, 'maxime', 0),
	(240, 'quaerat', 0),
	(241, '3902f43d-ccbe-4848-8640-e324d54f0018', 0),
	(242, '3d8a7f95-71be-42ef-964b-d5c5eb18fba6', 0),
	(243, 'bbfba04c-2d0d-45d2-9635-f82b672ee0f2', 0),
	(244, 'acbcf51e-48b8-4790-832a-6a36226f961c', 0),
	(245, 'c9791d8d-b6eb-46e8-99bf-ec244ed2f4b2', 0),
	(246, '8fbe3f2d-efe2-4834-a0e1-55c75c30ce92', 0),
	(247, '4306660e-f91a-4cc8-ae11-c7391fa3389d', 0),
	(248, '7e14e8e0-6ec5-40c9-a3c6-bcd162fba70f', 0),
	(249, '669c0898-ac5f-4caf-a1bf-015c8eda2d18', 0),
	(250, 'a4a2481e-2401-4766-8dfc-90a2fdff602c', 0),
	(251, '1ccfbe9a-3a06-47bb-b64a-4f40f16b4d33', 0),
	(252, 'b2c62620-c9a4-4683-a894-376eb44168cb', 0),
	(253, 'dc75ba29-e39c-4658-bca1-6cb4bdc5b40c', 0),
	(254, 'a5c6f459-def3-4f1a-8e2d-3d939dad1ef4', 0),
	(255, '9970f5b4-347a-499e-b82e-a2655adb1c26', 0),
	(256, 'a45d752b-ed01-4508-b191-bc118fa69bf3', 0),
	(257, 'a56f2627-a326-435b-8dcc-b0f0bec4e38e', 0),
	(258, '5e45fd4d-304e-4aed-af4b-81f26badb508', 0),
	(259, '70842137-750f-4a3a-8e5f-4817217f7908', 0),
	(260, '2988fa87-c630-4fdc-a674-ea1652da7e8f', 0);
/*!40000 ALTER TABLE `nationalities` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `visibility_id` int(11) NOT NULL,
  `creator_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext DEFAULT NULL,
  `media_id` bigint(20) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `enabled` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`post_id`),
  KEY `FK56o8t6bp5rtg7t7qxj8o6u47q` (`creator_id`),
  KEY `FKldx3bjuhem8yyl4jr19toswnl` (`media_id`),
  KEY `FKnb3qm20umjqi91s5va1yr71fo` (`visibility_id`),
  CONSTRAINT `FK56o8t6bp5rtg7t7qxj8o6u47q` FOREIGN KEY (`creator_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `FKldx3bjuhem8yyl4jr19toswnl` FOREIGN KEY (`media_id`) REFERENCES `media` (`media_id`),
  CONSTRAINT `FKnb3qm20umjqi91s5va1yr71fo` FOREIGN KEY (`visibility_id`) REFERENCES `visibilities` (`visibility_id`),
  CONSTRAINT `posts_media_media_id_fk` FOREIGN KEY (`media_id`) REFERENCES `media` (`media_id`),
  CONSTRAINT `posts_users_details_user_details_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `posts_visabilities_visability_id_fk` FOREIGN KEY (`visibility_id`) REFERENCES `visibilities` (`visibility_id`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.posts: ~188 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES
	(1, 1, 1, 'Cherries', 'Cherries are a nutritious, sweet treat that makes for a colorful addition in a variety of different dishes.', NULL, '2020-10-26 12:11:40', 0),
	(2, 1, 1, 'The Top 10 Healthiest Foods for Kids', 'You know it\'s better to feed your kids vegetables instead of ice cream. But, what are the healthiest foods for kids—and how do you get them to actually eat them?', 57, '2020-11-19 23:52:42', 1),
	(3, 1, 1, '7 Healthy Drinks for Kids (And 3 Unhealthy Ones)', 'While getting your child to eat nutritious foods can be challenging, finding healthy — yet appealing — beverages for your little ones can prove just as difficult.', NULL, '2020-10-26 12:10:08', 0),
	(4, 1, 1, 'Water', 'When your child tells you they’re thirsty, you should always offer water first.', NULL, '2020-10-26 12:13:22', 0),
	(5, 1, 1, 'Avocado', 'Avocados are an easy way to get healthy fats into your child\'s diet. ', NULL, '2020-10-26 12:20:50', 0),
	(6, 1, 2, 'Sweet Potato', 'Whether your kid is 6 months, 6 years old or 16 years old, sweet potatoes are appealing across the board (because they\'re sweet!)', NULL, '2020-10-26 14:26:56', 0),
	(7, 1, 1, 'Avocado', 'Avocados are an easy way to get healthy fats into your child\'s diet.', 4, '2020-10-26 15:33:15', 1),
	(8, 2, 3, 'Berries', 'Fresh berries make an excellent snack for kids or a great topping for yogurt.', 6, '2020-10-27 15:37:24', 0),
	(9, 2, 3, 'Berries', 'Fresh berries make an excellent snack for kids or a great topping for yogurt.', 7, '2020-10-27 17:01:14', 0),
	(10, 2, 3, 'Berries', 'Fresh berries make an excellent snack for kids or a great topping for yogurt.', 8, '2020-10-27 17:03:24', 0),
	(11, 1, 3, 'Sweet Potato', 'Whether your kid is 6 months, 6 years old or 16 years old, sweet potatoes are appealing across the board (because they\'re sweet!).', 9, '2020-10-27 17:16:49', 1),
	(12, 2, 2, 'Beans', 'Beans are a humble super food.', 10, '2020-10-27 18:48:33', 0),
	(13, 1, 5, 'Breakfast Recipes', 'You can share breakfast recipes.', 13, '2020-11-09 10:41:15', 0),
	(14, 1, 5, 'Breakfast recipes', 'Share your breakfast recipes here!', 14, '2020-11-09 11:05:46', 1),
	(15, 2, 4, 'Test', '', NULL, '2020-11-12 16:10:08', 0),
	(16, 1, 4, '11', '\r\nEngland is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England\r\nwhere b', NULL, '2020-11-13 13:20:57', 0),
	(17, 1, 4, '11', 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England\r\nwhere b\r\nEngland is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England. However, there is upland and mountainous terrain in the north (for example, the Lake District and Pennines) and in the west (for example, Dartmoor and the Shropshire Hills). The capital is London, which has the largest metropolitan area in both the United Kingdom and, prior to Brexit, the European Union.[nb 1] England\'s population of over 55 million comprises 84% of the population of the United Kingdom,[5] largely concentrated around London, the South East, and conurbations in the Midlands, the North West, the North East, and Yorkshire, which each developed as major industrial regions during the 19th century.[13]\r\n\r\nThe Kingdom of England – which after 1535 included Wales – ceased being a separate sovereign state on 1 May 1707, when the Acts of Union put into effect the terms agreed in the Treaty of Union the previous year, resulting in a political union with the Kingdom of Scotland to create the Kingdom of Great Britain.[14][15] In 1801, Great Britain was united with the Kingdom of Ireland (through another Act of Union) to become the United Kingdom of Great Britain and Ireland. In 1922 the Irish Free State seceded from the United Kingdom, leading to the latter being renamed the United Kingdom of Great Britain and Northern Ireland.[16\r\n', NULL, '2020-11-13 13:33:23', 0),
	(18, 1, 4, '11', 'The name "England" is derived from the Old English name Englaland, which means "land of the Angles".[17] The Angles were one of the Germanic tribes that settled in Great Britain during the Early Middle Ages. The Angles came from the Anglia peninsula in the Bay of Kiel area (present-day German state of Schleswig–Holstein) of the Baltic Sea.[18] The earliest recorded use of the term, as "Engla londe", is in the late-ninth-century translation into Old English of Bede\'s Ecclesiastical History of the English People. The term was then used in a different sense to the modern one, meaning "the land inhabited by the English", and it included English people in what is now south-east Scotland but was then part of the English kingdom of Northumbria. The Anglo-Saxon Chronicle recorded that the Domesday Book of 1086 covered the whole of England, meaning the English kingdom, but a few years later the Chronicle stated that King Malcolm III went "out of Scotlande into Lothian in Englaland", thus using it in the more ancient sense.[19]\r\n\r\nThe earliest attested reference to the Angles occurs in the 1st-century work by Tacitus, Germania, in which the Latin word Anglii is used.[20] The etymology of the tribal name itself is disputed by scholars; it has been suggested that it derives from the shape of the Angeln peninsula, an angular shape.[21] How and why a term derived from the name of a tribe that was less significant than others, such as the Saxons, came to be used for the entire country and its people is not known, but it seems this is related to the custom of calling the Germanic people in Britain Angli Saxones or English Saxons to distinguish them from continental Saxons (Eald-Seaxe) of Old Saxony between the Weser and Eider rivers in Northern Germany.[22] In Scottish Gaelic, another language which developed on the island of Great Britain, the Saxon tribe gave their name to the word for England (Sasunn);[23] similarly, the Welsh name for the English language is "Saesneg". A romantic name for England is Loegria, related to the Welsh word for England, Lloegr, and made popular by its use in Arthurian legend. Albion is also applied to England in a more poetic capacity,[24] though its original meaning is the island of Britain as a whole.', NULL, '2020-11-13 13:34:36', 0),
	(19, 1, 4, '11', 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England\r\nwhere b\r\nEngland is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and the Isle of Wight. It is the largest country of the British Isles.\r\n\r\nThe area now called England was first inhabited by modern humans during the Upper Paleolithic period, but takes its name from the Angles, a Germanic tribe deriving its name from the Anglia peninsula, who settled during the 5th and 6th centuries. England became a unified state in the 10th century, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world.[10] The English language, the Anglican Church, and English law – the basis for the common law legal systems of many other countries around the world – developed in England, and the country\'s parliamentary system of government has been widely adopted by other nations.[11] The Industrial Revolution began in 18th-century England, transforming its society into the world\'s first industrialised nation.[12]\r\n\r\nEngland\'s terrain is chiefly low hills and plains, especially in central and southern England. However, there is upland and mountainous terrain in the north (for example, the Lake District and Pennines) and in the west (for example, Dartmoor and the Shropshire Hills). The capital is London, which has the largest metropolitan area in both the United Kingdom and, prior to Brexit, the European Union.[nb 1] England\'s population of over 55 million comprises 84% of the population of the United Kingdom,[5] largely concentrated around London, the South East, and conurbations in the Midlands, the North West, the North East, and Yorkshire, which each developed as major industrial regions during the 19th century.[13]\r\n\r\nThe Kingdom of England – which after 1535 included Wales – ceased being a separate sovereign state on 1 May 1707, when the Acts of Union put into effect the terms agreed in the Treaty of Union the previous year, resulting in a political union with the Kingdom of Scotland to create the Kingdom of Great Britain.[14][15] In 1801, Great Britain was united with the Kingdom of Ireland (through another Act of Union) to become the United Kingdom of Great Britain and Ireland. In 1922 the Irish Free State seceded from the United Kingdom, leading to the latter being renamed the United Kingdom of Great Britain and Northern Ireland.[16The earliest known evidence of human presence in the area now known as England was that of Homo antecessor, dating to approximately 780,000 years ago. The oldest proto-human bones discovered in England date from 500,000 years ago.[25] Modern humans are known to have inhabited the area during the Upper Paleolithic period, though permanent settlements were only established within the last 6,000 years.[26][27] After the last ice age only large mammals such as mammoths, bison and woolly rhinoceros remained. Roughly 11,000 years ago, when the ice sheets began to recede, humans repopulated the area; genetic research suggests they came from the northern part of the Iberian Peninsula.[28] The sea level was lower than now and Britain was connected by land bridge to Ireland and Eurasia.[29] As the seas rose, it was separated from Ireland 10,000 years ago and from Eurasia two millennia later.\r\n\r\nThe Beaker culture arrived around 2,500 BC, introducing drinking and food vessels constructed from clay, as well as vessels used as reduction pots to smelt copper ores.[30] It was during this time that major Neolithic monuments such as Stonehenge and Avebury were constructed. By heating together tin and copper, which were in abundance in the area, the Beaker culture people made bronze, and later iron from iron ores. The development of iron smelting allowed the construction of better ploughs, advancing agriculture (for instance, with Celtic fields), as well as the production of more effective weapons.[31]\r\n\r\nDuring the Iron Age, Celtic culture, deriving from the Hallstatt and La T?ne cultures, arrived from Central Europe. Brythonic was the spoken language during this time. Society was tribal; according to Ptolemy\'s Geographia there were around 20 tribes in the area. Earlier divisions are unknown because the Britons were not literate. Like other regions on the edge of the Empire, Britain had long enjoyed trading links with the Romans. Julius Caesar of the Roman Republic attempted to invade twice in 55 BC; although largely unsuccessful, he managed to set up a client king from the Trinovantes.\r\n', NULL, '2020-11-13 13:35:49', 0),
	(20, 1, 1, '11', '', NULL, '2020-11-13 13:48:08', 0),
	(21, 1, 1, 'England is a country that is part of the United Kin', '', NULL, '2020-11-13 13:52:28', 0),
	(22, 1, 1, '11', 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with with with ', NULL, '2020-11-13 13:53:59', 0),
	(23, 1, 4, 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with some a', '', NULL, '2020-11-13 15:15:50', 0),
	(24, 1, 2, 'Test video mp4', '', 30, '2020-11-15 00:57:09', 0),
	(25, 1, 2, 'Test video mp4', '', 31, '2020-11-15 01:18:59', 0),
	(26, 1, 2, 'Test pucture', 'bla bla', 32, '2020-11-15 01:48:09', 0),
	(27, 2, 4, 'Changed test like', '', NULL, '2020-11-16 09:32:12', 0),
	(28, 1, 1, '???', '', NULL, '2020-11-16 01:48:05', 0),
	(29, 1, 1, '?????', '', NULL, '2020-11-16 01:54:58', 0),
	(30, 1, 1, 'test video', '', 33, '2020-11-17 14:32:15', 0),
	(31, 1, 1, 'Test video mp4', '', 34, '2020-11-17 14:34:32', 0),
	(32, 1, 1, 'Test video mp4', '', 35, '2020-11-17 14:35:17', 0),
	(33, 1, 1, '00', '', 36, '2020-11-17 14:45:04', 0),
	(34, 1, 1, '01', '', 37, '2020-11-17 14:46:50', 0),
	(35, 1, 1, '12', '', 38, '2020-11-17 14:49:21', 0),
	(36, 2, 2, 'milk', 'gbiulgligiu', 39, '2020-11-19 12:04:50', 0),
	(37, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 40, '2020-11-19 12:52:55', 0),
	(38, 1, 1, 'Test', '', 41, '2020-11-19 13:20:19', 0),
	(39, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 42, '2020-11-19 13:24:24', 0),
	(40, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 43, '2020-11-19 13:26:34', 0),
	(41, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 44, '2020-11-19 13:34:01', 0),
	(42, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 45, '2020-11-19 13:50:41', 0),
	(43, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 46, '2020-11-19 13:53:02', 0),
	(44, 2, 2, 'CONNECTIONS_TITLE_POST', 'CONNECTIONS_POST_DESCRIPTION', 47, '2020-11-19 14:03:29', 0),
	(45, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 48, '2020-11-19 14:19:21', 0),
	(46, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 49, '2020-11-19 14:24:10', 0),
	(47, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 50, '2020-11-19 15:11:22', 0),
	(48, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 51, '2020-11-19 15:25:17', 0),
	(49, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 52, '2020-11-19 15:27:59', 0),
	(50, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 53, '2020-11-19 15:51:01', 0),
	(51, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 54, '2020-11-19 16:05:56', 0),
	(52, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 55, '2020-11-19 18:46:02', 0),
	(53, 1, 1, '7 Healthy Drinks for Kids ', 'Most children have a sweet tooth and are prone to asking for sugary beverages. However, guiding them towards more balanced options is important for their overall health.', 58, '2020-11-20 00:00:14', 1),
	(54, 1, 2, 'Water', 'When your child tells you they’re thirsty, you should always offer water first.', 59, '2020-11-20 00:02:37', 0),
	(55, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 60, '2020-11-20 00:20:13', 0),
	(56, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 61, '2020-11-20 00:14:39', 0),
	(57, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 62, '2020-11-20 02:31:20', 0),
	(58, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 63, '2020-11-20 10:09:13', 0),
	(59, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 64, '2020-11-20 10:19:15', 0),
	(60, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 65, '2020-11-20 23:13:21', 0),
	(61, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 66, '2020-11-21 13:26:23', 0),
	(62, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 67, '2020-11-21 13:30:30', 0),
	(63, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 68, '2020-11-21 13:32:41', 0),
	(64, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 69, '2020-11-21 13:51:04', 0),
	(65, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 70, '2020-11-21 13:52:46', 0),
	(66, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 71, '2020-11-21 14:04:00', 0),
	(67, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 72, '2020-11-21 14:12:58', 0),
	(68, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 73, '2020-11-21 21:45:37', 0),
	(69, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 74, '2020-11-21 23:08:19', 0),
	(70, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 75, '2020-11-21 23:11:29', 0),
	(71, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 76, '2020-11-21 23:14:59', 0),
	(72, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 77, '2020-11-21 23:16:44', 0),
	(73, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 78, '2020-11-21 23:24:51', 0),
	(74, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 79, '2020-11-21 23:27:45', 0),
	(75, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 80, '2020-11-21 23:31:49', 0),
	(76, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 81, '2020-11-21 23:33:57', 0),
	(77, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 82, '2020-11-21 23:36:52', 0),
	(78, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 83, '2020-11-21 23:39:26', 0),
	(79, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 84, '2020-11-21 23:47:12', 0),
	(80, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 85, '2020-11-21 23:48:42', 0),
	(81, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 86, '2020-11-21 23:51:14', 0),
	(82, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 87, '2020-11-22 00:00:15', 0),
	(83, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 88, '2020-11-22 00:06:03', 0),
	(84, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 89, '2020-11-22 00:51:51', 0),
	(85, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 90, '2020-11-22 00:55:49', 0),
	(86, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 91, '2020-11-22 01:03:24', 0),
	(87, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 92, '2020-11-22 01:04:51', 0),
	(88, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 93, '2020-11-22 01:18:56', 0),
	(89, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 94, '2020-11-22 11:03:00', 0),
	(90, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 95, '2020-11-22 11:06:11', 0),
	(91, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 96, '2020-11-22 11:21:59', 0),
	(92, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 97, '2020-11-22 11:58:26', 0),
	(93, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 98, '2020-11-22 12:04:06', 0),
	(94, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 99, '2020-11-22 12:10:38', 0),
	(95, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 100, '2020-11-22 12:14:12', 0),
	(96, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 101, '2020-11-22 12:20:14', 0),
	(97, 1, 2, 'Like Post', 'Like me!', NULL, '2020-11-22 12:25:24', 0),
	(98, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 102, '2020-11-22 12:27:19', 0),
	(99, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 103, '2020-11-22 12:30:43', 0),
	(100, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 104, '2020-11-22 14:46:16', 0),
	(101, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 105, '2020-11-22 14:49:38', 0),
	(102, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 106, '2020-11-22 23:52:40', 0),
	(103, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 107, '2020-11-22 23:49:03', 0),
	(104, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 108, '2020-11-22 23:53:38', 0),
	(105, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 109, '2020-11-23 00:15:51', 0),
	(106, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 110, '2020-11-23 00:25:51', 0),
	(107, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 111, '2020-11-23 00:28:08', 0),
	(108, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 112, '2020-11-23 00:42:28', 0),
	(109, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 113, '2020-11-23 01:26:17', 0),
	(110, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 114, '2020-11-23 01:37:23', 0),
	(111, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 115, '2020-11-23 01:48:04', 0),
	(112, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 116, '2020-11-23 01:51:20', 0),
	(113, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 117, '2020-11-23 01:51:57', 0),
	(114, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 118, '2020-11-23 01:56:35', 0),
	(115, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 119, '2020-11-23 02:04:19', 0),
	(116, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 120, '2020-11-23 02:07:45', 0),
	(117, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 121, '2020-11-23 02:11:11', 0),
	(118, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 122, '2020-11-23 02:14:16', 0),
	(119, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 123, '2020-11-23 02:17:54', 0),
	(120, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 126, '2020-11-23 09:41:19', 0),
	(121, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 127, '2020-11-23 11:17:13', 0),
	(122, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 128, '2020-11-23 11:19:53', 0),
	(123, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 129, '2020-11-23 11:29:31', 0),
	(124, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 130, '2020-11-23 11:25:47', 0),
	(125, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 131, '2020-11-23 11:33:29', 0),
	(126, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 132, '2020-11-23 11:38:25', 0),
	(127, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 145, '2020-11-23 22:42:34', 0),
	(128, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 146, '2020-11-23 22:51:23', 0),
	(129, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 147, '2020-11-23 22:55:30', 0),
	(130, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 148, '2020-11-23 22:58:15', 0),
	(131, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 150, '2020-11-23 23:05:32', 0),
	(132, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 156, '2020-11-23 23:53:44', 0),
	(133, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 157, '2020-11-24 00:06:44', 0),
	(134, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 159, '2020-11-24 00:11:20', 0),
	(135, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 169, '2020-11-25 15:57:46', 0),
	(136, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 181, '2020-11-26 02:19:23', 0),
	(137, 1, 1, 'New post', NULL, NULL, '2020-11-26 19:51:06', 0),
	(138, 1, 1, 'New post', NULL, NULL, '2020-11-26 19:52:37', 0),
	(139, 1, 1, 'New post', NULL, NULL, '2020-11-26 20:08:17', 0),
	(140, 1, 1, 'New post', NULL, NULL, '2020-11-26 20:17:23', 0),
	(141, 1, 1, 'New post', NULL, NULL, '2020-11-26 20:18:10', 0),
	(142, 2, 5, 'Connection post', 'Can u see me ?', 190, '2020-11-27 10:18:57', 0),
	(143, 1, 1, 'New post', NULL, NULL, '2020-11-27 12:16:22', 0),
	(144, 1, 1, 'New post', NULL, 191, '2020-11-27 13:10:45', 0),
	(145, 1, 1, 'New post', NULL, NULL, '2020-11-27 13:38:00', 0),
	(146, 1, 1, 'Water', NULL, NULL, '2020-11-27 13:40:46', 0),
	(147, 1, 1, 'Water', NULL, 192, '2020-11-27 13:41:33', 0),
	(148, 1, 1, 'Water', NULL, 193, '2020-11-27 13:57:40', 0),
	(149, 1, 1, 'Water', NULL, 194, '2020-11-27 15:20:48', 0),
	(150, 1, 1, 'Water', NULL, 195, '2020-11-27 15:47:55', 0),
	(151, 1, 13, 'Test', '', NULL, '2020-11-27 15:53:36', 0),
	(152, 1, 1, 'Water', NULL, 196, '2020-11-27 15:55:55', 0),
	(153, 1, 1, 'Water', NULL, 197, '2020-11-27 15:57:33', 0),
	(154, 1, 1, 'Water', NULL, 198, '2020-11-27 15:58:54', 0),
	(155, 1, 1, 'Water', NULL, 199, '2020-11-27 17:22:33', 0),
	(156, 1, 1, 'Water', NULL, 200, '2020-11-27 17:22:35', 0),
	(157, 1, 1, 'Water', NULL, 201, '2020-11-27 17:23:51', 0),
	(158, 1, 1, 'Water', NULL, 202, '2020-11-27 17:23:53', 0),
	(159, 1, 1, 'Water', NULL, 203, '2020-11-27 19:36:00', 0),
	(160, 1, 1, 'Water', NULL, 204, '2020-11-27 19:36:01', 0),
	(161, 1, 1, 'Water', NULL, 206, '2020-11-27 19:43:55', 0),
	(162, 1, 1, 'Water', NULL, 207, '2020-11-27 19:43:56', 0),
	(163, 1, 1, 'Water', NULL, 208, '2020-11-27 19:45:14', 0),
	(164, 1, 1, 'Water', NULL, 209, '2020-11-27 19:45:15', 0),
	(165, 1, 1, 'Water', NULL, 210, '2020-11-27 19:46:32', 0),
	(166, 1, 1, 'Water', NULL, 211, '2020-11-27 19:46:34', 0),
	(167, 1, 1, 'Water', NULL, 212, '2020-11-27 20:54:19', 0),
	(168, 1, 1, 'Water', NULL, 213, '2020-11-27 20:54:21', 0),
	(169, 1, 1, 'Water', NULL, 214, '2020-11-27 20:55:42', 0),
	(170, 1, 1, 'Water', NULL, 215, '2020-11-27 20:55:45', 0),
	(171, 1, 1, 'Water', NULL, 216, '2020-11-27 20:58:01', 0),
	(172, 1, 1, 'Water', NULL, 217, '2020-11-27 20:58:02', 0),
	(173, 1, 1, 'Water', NULL, 218, '2020-11-27 20:59:21', 0),
	(174, 1, 1, 'Water', NULL, 219, '2020-11-27 20:59:22', 0),
	(175, 1, 1, 'Water', NULL, 220, '2020-11-27 21:01:10', 0),
	(176, 1, 1, 'Water', NULL, 221, '2020-11-27 21:01:12', 0),
	(177, 1, 1, 'Water', NULL, 222, '2020-11-27 21:06:34', 0),
	(178, 1, 1, 'Water', NULL, 223, '2020-11-27 21:06:36', 0),
	(179, 1, 1, 'Water', NULL, 224, '2020-11-27 21:30:04', 0),
	(180, 1, 1, 'Water', NULL, 225, '2020-11-27 21:30:05', 0),
	(181, 1, 1, 'Water', NULL, 227, '2020-11-28 17:38:37', 0),
	(182, 1, 1, 'Water', NULL, 228, '2020-11-28 17:38:39', 0),
	(183, 1, 1, 'Water', NULL, 229, '2020-11-28 21:08:05', 0),
	(184, 1, 1, 'Water', NULL, 230, '2020-11-28 21:08:07', 0),
	(185, 1, 1, 'Water', NULL, 231, '2020-11-28 22:48:05', 0),
	(186, 1, 1, 'Water', NULL, 232, '2020-11-28 22:48:06', 0),
	(187, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 235, '2020-11-28 23:29:34', 0),
	(188, 2, 2, 'Milk', 'Milk helps build strong bones because it\'s full of calcium and vitamin', 237, '2020-11-28 23:57:05', 0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.posts_categories
CREATE TABLE IF NOT EXISTS `posts_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKj9cwvxrsmk65ie2palq6dvrdb` (`category_id`),
  KEY `FKq4402eed597exuj50g73c7kmg` (`post_id`),
  CONSTRAINT `FKj9cwvxrsmk65ie2palq6dvrdb` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `FKq4402eed597exuj50g73c7kmg` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `posts_categories_categories_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `posts_categories_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.posts_categories: ~5 rows (approximately)
DELETE FROM `posts_categories`;
/*!40000 ALTER TABLE `posts_categories` DISABLE KEYS */;
INSERT INTO `posts_categories` (`id`, `post_id`, `category_id`) VALUES
	(2, 3, 2),
	(4, 4, 2),
	(8, 2, 1),
	(9, 53, 5),
	(10, 54, 5);
/*!40000 ALTER TABLE `posts_categories` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.posts_likes
CREATE TABLE IF NOT EXISTS `posts_likes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_details_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_likes_pk_2` (`user_details_id`,`post_id`),
  KEY `FKimxtd6dl39nmu9x0snqm6mu1g` (`post_id`),
  CONSTRAINT `FK2fr813oum8ffogytktlwnihos` FOREIGN KEY (`user_details_id`) REFERENCES `users_details` (`user_details_id`),
  CONSTRAINT `FKimxtd6dl39nmu9x0snqm6mu1g` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `posts_likes_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `posts_likes_users_details_user_details_id_fk` FOREIGN KEY (`user_details_id`) REFERENCES `users_details` (`user_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.posts_likes: ~7 rows (approximately)
DELETE FROM `posts_likes`;
/*!40000 ALTER TABLE `posts_likes` DISABLE KEYS */;
INSERT INTO `posts_likes` (`id`, `user_details_id`, `post_id`) VALUES
	(22, 1, 14),
	(15, 2, 7),
	(30, 2, 11),
	(59, 3, 2),
	(16, 3, 7),
	(31, 5, 11),
	(20, 5, 14);
/*!40000 ALTER TABLE `posts_likes` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `statuses_type_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.statuses: ~2 rows (approximately)
DELETE FROM `statuses`;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`status_id`, `type`, `enabled`) VALUES
	(1, 'connected', 1),
	(2, 'sent request', 1);
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.users
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(100) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.users: ~76 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('10@abv.bg', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('10@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('11@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('11@outlook.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('12', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('12@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('1@abv.bg', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('1@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('1@outlook.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('1@yahoo.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 0),
	('4@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 0),
	('a@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 0),
	('aaa@abv.bg', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 0),
	('aaaa@abv.bg', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 0),
	('AbXFJMJDVE@gmail.com', '$2a$10$YBrGBXfZ.IQ5Y.fxcPNCz.YBgaFsbazXwK.imdxyZymyXdJ63PlKG', 0),
	('ArjsKQxvvh@gmail.com', '$2a$10$1LePyB7pWCO31kLj/cy73uTt2.sGmaaUCsOElivSb9Ug1uBotmx02', 0),
	('b@gmail.com', '$2a$10$5BuAvPlNNcj68CxmKwDbLOFBdPlBzZrvxWsweZGtcixMp36WWxRAq', 0),
	('bbb@gmail.com', '$2a$10$ikn.XDGLrEPI2RHTEMiwq.34gvso2UXX8l3iCS1MkKLTCjF/9q0mK', 0),
	('bpQXz@gmail.com', '$2a$10$tz1wDhQdgTPH9siYJMKFROWSSf3tCsZEw4gmHV7vTETl.FnEN98FO', 0),
	('bRcPe@gmail.com', '$2a$10$gBwP8NRsTXq3r3BoFIYAu.q8YHdG/TwueKqBc5ao.chltcJd3p5oa', 0),
	('BvXtj@gmail.com', '$2a$10$6h/24/Wk9YXS1r8DhnEimu5YZ3av9SZIzv5/6wm/Uer6q0RMb3lLW', 0),
	('c@gmail.com', '$2a$10$2Rt6QhQDpCCbJMT4dtAReetfuYJjWCpgCI7LJALWGxfCJGJxYDwKO', 0),
	('ccc@gmail.com', '$2a$10$F3m3YqihWEKr/sUbV8YIAu8Hte/4vahi1vAGb11CsST31uOESPMti', 0),
	('CZtxipdhBC@gmail.com', '$2a$10$rfxTUSnSSt4YT5JiFeZFeOtRufrtX/J759eA7oLrggASe.LFFTdqy', 0),
	('d@gmail.com', '$2a$10$8cI3Hq0ZbK5iz2DVkPNJGejMK7/yb/EBbXuUTGeTxXS3HIoHUgHri', 0),
	('deleted_user@gmail.com', '$2a$10$5QDb/Rmgk9LhW3ZC6yp9EerrmeJpcH6qon246DeG/TqXgyyTFeNg.', 0),
	('DlOYi@gmail.com', '$2a$10$VWX0sy5JBTpWwww77/4m4uomJZi4SF4If.ZIdFI8G58YJT3zeh9xO', 0),
	('EASHi@gmail.com', '$2a$10$vbK8ABNREucK5EnOCj8dN.RMAypFc6CNqIHp3ubQxrDsnJuQfNde2', 0),
	('ee@gmail.com', '$2a$10$meYNn2paq3km2qDDXp14UOaYrAFS/KsCdZ.XgMCn8.nVp6MUfr3pK', 0),
	('egcaF@gmail.com', '$2a$10$W1ycV9n5Iwvu4PFENQJy.eKNKWLv5tzYJr7zhzSJHZZStANnsXSHu', 0),
	('evaHF@gmail.com', '$2a$10$QeH2sxh7ECMSoTWzA4QMwesqnfwKn9hDJO42oXASbZG0RZAOW//iS', 0),
	('finalprojectnarnia@gmail.com', '$2a$10$9jomFqAhy410O.gbmSmt7eE8t1mq6g4dr7cteQgaXOaNfE.yh5jG2', 1),
	('fyAMmBmyjh@gmail.com', '$2a$10$vOWq0TVPWN0VYmxJLwo2CukEUC84NTJNC4I.6QkdwzwmGQExuqYVW', 0),
	('hFPwn@gmail.com', '$2a$10$Pb.JCIAwclkC6uf6y7bZKOoSMczs.dYYo0g8Un7LOfZNHypQGVekC', 0),
	('hsgMMLWAxY@gmail.com', '$2a$10$gHvJlGSxXqezU4YuiuTd3OqXvs7weweMnmf1SF/HyBpjW5t0MBou2', 0),
	('irka_1982@abv.bg', '$2a$10$bFq4ZQy/39lRaeFGZJeTiO4LzF8DotYfBbUVHiVjbGE80RnUwmpg.', 1),
	('JXYcQ@gmail.com', '$2a$10$MfUK4dq9XB8rz5FV6eZWFuz8K6tyyX.CxepdKlFvrHV9LQVqPuFXK', 0),
	('KHCXP@gmail.com', '$2a$10$Ac7doSTaPNZdBQ2bxR.SP.mZzRsXlavnlSgXH33.izUsDBR2Jw3S2', 0),
	('KNtYd@gmail.com', '$2a$10$Mx0rHPvPpNfxD66/6Z24oOGSZ8RzNUJVHBC0WyzpBffu4ZFqjCrDe', 0),
	('kZqYx@gmail.com', '$2a$10$Y/tELXkQ0l3YCc/oaG8f0.SlTcEx2EFEr2971hPDiVNLrCl8AXIZm', 0),
	('LEfTZ@gmail.com', '$2a$10$Bi3GFhsQNvDc6XgNpsv1.e4kcGsPpB9/KrxY/.2mzhZ39Zui7bwTW', 0),
	('LGiNV@gmail.com', '$2a$10$n/TohWTbWE/w6YIFR5k.f.uJb0PFVQtr7je22acgXtUo8qdrv7xZG', 0),
	('LopPL@gmail.com', '$2a$10$OzdqK2nlRqhhtAioXxVdEu7ksdyEmvTt1ndCKuZ85e6D4saLh2v7O', 0),
	('LQAkW@gmail.com', '$2a$10$JDK2sxvbv8Qu6qFhy4WzIO.IjTAX6oPcw/BtT2rbzYAixjFJSsC3u', 0),
	('LUCVS@gmail.com', '$2a$10$uEx6CUqvBgDCJ7Wmna/BwOSjfqIA3Gwu4RxsL/R.amvS1yoYeUJh.', 0),
	('martin.vlachkov22@gmail.com', '$2a$10$KzOGT9rc7Ui7S2cDQPCzJuzzFbPnVXMegsLjyi7RwmqCRibYbHURm', 1),
	('marto221@gmail.com', '$2a$10$PVDqSdDBQ.JctqdPIIEIEO8KG0ZRLyeGaCa97cmCIJiMurzZiZNru', 0),
	('marto22@gmail.com', '$2a$10$7kSnRNUwUVA2wnF5JjHapOFcs48rOPi106tHiojiPaw28iI8D2W3K', 0),
	('marto@abv.bg', '$2a$10$b37RMvSo6f70VOSC3P3lKOeWCEeNCsEmDEg7dUIppblxf0okbWYw2', 0),
	('mjOvHbncFm@gmail.com', '$2a$10$92wx/srQNWVaXlvn/5.KPOC/5dK0reO1jpMUslZRzJPbHS3J03D2G', 0),
	('nAKPV@gmail.com', '$2a$10$HNEkhWDjtnKuhBE1boLSOex.QQOhszuUwHX8gOQLQnSqHjFx6QqFW', 0),
	('nevenamalinova@outlook.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('nevina_82@abv.bg', '$2a$10$9nZbXhOBwKvMl8aClQEcJu.D88wQrIQTsN/aB.7wAER.HS0Yc/eGi', 1),
	('nluVw@gmail.com', '$2a$10$vj.28s7PSK3oYDwQRN/T2OpybrITAuDqvR9qeGOjl45I0zZaV3BtC', 0),
	('NPwaOvqnCg@gmail.com', '$2a$10$APUvu3z66mj2FgqD7E//6elx3bMhjd64s7C2IqFzMcxwydnlwqafq', 0),
	('NWpDv@gmail.com', '$2a$10$JGKvEVKJkIyFUgFkHWMTbedOd8cxEZbbOPkOB6aztBLEi5cL4SLay', 0),
	('oeweL@gmail.com', '$2a$10$MIxJAaRr6MJSFMrApLazhenx9dpGUo7C5NGJz6UaqqHRvPek63D1q', 0),
	('PkkXd@gmail.com', '$2a$10$vzKW1uIeEPJsamT7hda/7OzY/E6oTCg8UD0JaMK.YYbF85msn2l5u', 0),
	('qTkUK@gmail.com', '$2a$10$jaMlBwJDdUDwTc2XShpUKOvPKYnfWzG6Hiho6xNRPx9j1qtWgDXky', 0),
	('renbSpQMCV@gmail.com', '$2a$10$55PKwLN373bdT6r6IE4D.OZPdo6P2ExV/Oy7W3K/hy4eKl7IHIFVq', 0),
	('RQwxu@gmail.com', '$2a$10$SBhpO5s52bzWafA/NTKKfeEh.YuHGxS7mCrJy/RngxeCvQbreliZe', 0),
	('sUFFkfpNIB@gmail.com', '$2a$10$WjlbGKjn6g0l4LDg6G1hJOfF8RJKpWk/kB5tPGtp1fhFxy6i1NX2i', 0),
	('test1234@gmail.com', '$2a$10$M/Bp4dbfPJLyeMpmXwu3Xe1oO0oEa.B0JbXz3xUCwLwdKMZ0VrSFy', 0),
	('test123@gmail.com', '$2a$10$NOIkNodniNFMn.03nyDbP.aJxQiB1YL6crmff7H/ruMVJsjqMiPRm', 1),
	('tpOXs@gmail.com', '$2a$10$AIClENEetBCYcv2jogGMye4TSChp0eo4jr6woa4WW4chJwZAgwYlG', 0),
	('UbyKN@gmail.com', '$2a$10$rnnw9nlifyxInB8uvtF.sOIThBuMQQJ6NkhYRAPUXrLT7rJTHjoiW', 0),
	('uFJiXukDPB@gmail.com', '$2a$10$6dHq1MGyVoYe9HncdINAHOvqyb2M5KPQ5GySSLx6YAC9h0/MHJW6G', 0),
	('vwHMvSpcbp@gmail.com', '$2a$10$xuwXzO3f9Zth6DwPHatPQOVcoT0j8FL8lTvEm61wJ7XWz7xF65eRu', 0),
	('WuLgk@gmail.com', '$2a$10$hzVRQZpHHQ.8ToSkRDTiIeYtGKTm5JRE/UMIN4pmS4YHragu.XMz2', 0),
	('xaKyM@gmail.com', '$2a$10$FtOfVBJP/wxJjck29MtIR.vRM46Zq1YuCiXXfXoy77SbzIG5udjFi', 0),
	('XWjKp@gmail.com', '$2a$10$LKhIrohBFVKbGk6Oif2Xhe/S9Ij0XlT.sWc589STPLTHTiNIFG81S', 0),
	('YEMAiljUOO@gmail.com', '$2a$10$ODIY74IrqYDjUM8Ho2rIeumOR9y5Je0euE28wWEtItm.fzZwenha.', 0),
	('YzMPKrIgXZ@gmail.com', '$2a$10$YmLDqkGnW9GwaPvMbrzaNe1zF10ss5HueZvlTJaYTQRYQBu1Z3jae', 0),
	('ZeXet@gmail.com', '$2a$10$vLylLfcDkLTGZMatg4xY7O/Hz64sBYVU..b3Ypa6o1eiSIjEJ9dc6', 0),
	('ztSDA@gmail.com', '$2a$10$4ZIYYq7Dzg5484zcp1gnh.m2iPOjMroPNgIK/QG.931XepbvPdRNi', 0),
	('ZxFam@gmail.com', '$2a$10$cgRCEfTbMfaRDwfG362vk.rtyxIJIzfSVFcaEUsoaCppm/1bmSbMO', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.users_details
CREATE TABLE IF NOT EXISTS `users_details` (
  `user_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `picture_id` bigint(20) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_details_id`),
  UNIQUE KEY `users_details_email_uindex` (`email`),
  KEY `FKrp39imaypr744lot1bjqm370n` (`gender_id`),
  KEY `FKbed4h8p0213jxov6mslkqh0oq` (`nationality_id`),
  KEY `FK83sockx4nqp1v2q7ufie1y7eu` (`picture_id`),
  CONSTRAINT `FK83sockx4nqp1v2q7ufie1y7eu` FOREIGN KEY (`picture_id`) REFERENCES `media` (`media_id`),
  CONSTRAINT `FKbed4h8p0213jxov6mslkqh0oq` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`nationality_id`),
  CONSTRAINT `FKrp39imaypr744lot1bjqm370n` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
  CONSTRAINT `users_details_gender_gender_id_fk` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
  CONSTRAINT `users_details_medias_media_id_fk` FOREIGN KEY (`picture_id`) REFERENCES `media` (`media_id`),
  CONSTRAINT `users_details_nationalities_nationality_id_fk` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`nationality_id`),
  CONSTRAINT `users_details_users_username_fk` FOREIGN KEY (`email`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.users_details: ~75 rows (approximately)
DELETE FROM `users_details`;
/*!40000 ALTER TABLE `users_details` DISABLE KEYS */;
INSERT INTO `users_details` (`user_details_id`, `email`, `first_name`, `last_name`, `picture_id`, `description`, `age`, `gender_id`, `nationality_id`, `enabled`) VALUES
	(1, 'finalprojectnarnia@gmail.com', 'User1', 'Admin', 1, '', 38, 2, 29, 1),
	(2, 'nevina_82@abv.bg', 'User2', 'Regular', 2, '', 51, 2, 4, 1),
	(3, 'irka_1982@abv.bg', 'User3', 'Regular', 3, '', 38, 2, 29, 1),
	(4, 'nevenamalinova@outlook.com', 'User4', 'Regular', 5, 'Beyonc? Giselle Knowles-Carter (born September 4, 1981) is an American singer and actress. Her contributions to music, dance, and fashion have established her as one of the most influential artists in the history of popular music. Born and raised in Houston, Texas, Beyonc? performed in various singing and dancing competitions as a child. She rose to fame in the late 1990s as the lead singer of Destiny\'s Child, one of the best-selling girl groups of all time. During Destiny\'s Child\'s hiatus, Bey2', 39, 2, 4, 1),
	(5, 'martin.vlachkov22@gmail.com', 'User5', 'Regular', 11, 'Change me', 22, 1, 13, 1),
	(6, 'test123@gmail.com', 'User6', 'Regular', 12, NULL, 0, NULL, NULL, 0),
	(7, '1@abv.bg', '11', '11', 15, '', 18, NULL, NULL, 0),
	(8, '1@outlook.com', '22', '22', 16, '', 1096, NULL, NULL, 0),
	(9, '4@gmail.com', '33', '33', 17, '', 0, NULL, NULL, 0),
	(10, '1@gmail.com', '@@', '@@', 18, 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and ne ', 0, NULL, NULL, 0),
	(11, '10@gmail.com', 'User7', 'Regular', 19, '', 77, NULL, NULL, 0),
	(12, '11@gmail.com', 'User8', 'Regular', 20, '', 88, NULL, NULL, 0),
	(13, '12@gmail.com', 'User9', 'Regular', 21, '', 99, NULL, NULL, 0),
	(14, '10@abv.bg', 'Deleting ', 'Regular', 22, 'England is a country that is part of the United Kingdom.[7][8][9] It shares land borders with Wales to its west and Scotland to its north. The Irish Sea lies northwest of England and the Celtic Sea to the southwest. England is separated from continental Europe by the North Sea to the east and the English Channel to the south. The country covers five-eighths of the island of Great Britain, which lies in the North Atlantic, and includes over 100 smaller islands, such as the Isles of Scilly and nk', 25, NULL, NULL, 0),
	(15, '11@outlook.com', '##', '@&', 23, '', 18, NULL, NULL, 0),
	(16, '1@yahoo.com', '%%', '>?', 24, '', 265, NULL, NULL, 0),
	(17, 'a@gmail.com', 'a1', 'a1', 25, '', 0, NULL, NULL, 0),
	(18, 'b@gmail.com', 'oo', '00', 26, '', 0, NULL, NULL, 0),
	(19, 'c@gmail.com', 'cc', 'cc', 27, '', 0, NULL, NULL, 0),
	(20, 'd@gmail.com', 'dd', 'dd', 28, '', 0, NULL, NULL, 0),
	(21, 'ee@gmail.com', 'Narniaqa22@111111111111111111111111111111111111111', 'Narniaqa22@111111111111111111111111111111111111111', 29, '', 0, NULL, NULL, 0),
	(22, 'aaa@abv.bg', 'User1', 'Admin', 124, '', 0, NULL, NULL, 0),
	(23, 'aaaa@abv.bg', 'User1', 'Admin', 125, '', 0, NULL, NULL, 0),
	(24, 'bbb@gmail.com', 'User1', 'Admin', 133, '', 0, NULL, NULL, 0),
	(25, 'ccc@gmail.com', 'User1', 'Admin', 134, '', 0, NULL, NULL, 0),
	(26, 'marto22@gmail.com', 'Martin', 'Vlachkov', 135, 'I like testing!', 350, 2, 4, 0),
	(27, 'marto@abv.bg', 'martin', 'vlachkov', 136, '', 0, NULL, NULL, 0),
	(28, 'YzMPKrIgXZ@gmail.com', 'lAAgagliVG', 'JLgJmgZxVf', 137, '', 0, NULL, NULL, 0),
	(29, 'uFJiXukDPB@gmail.com', 'wRvgazcWwM', 'qxaJftsLqj', 138, '', 0, NULL, NULL, 0),
	(30, 'vwHMvSpcbp@gmail.com', 'RjtfdUqBAg', 'QRFNVetHRj', 139, '', 0, NULL, NULL, 0),
	(31, 'evaHF@gmail.com', 'Martin', 'Vlachkov', 140, 'I like testing!', 350, 2, 4, 0),
	(32, 'XWjKp@gmail.com', 'Martin', 'Vlachkov', 141, 'I like testing!', 350, 2, 125, 0),
	(33, 'ZxFam@gmail.com', 'Martin', 'Vlachkov', 142, 'I like testing!', 350, 2, 4, 0),
	(34, 'PkkXd@gmail.com', 'Martin', 'Vlachkov', 143, 'I like testing!', 350, 2, 4, 0),
	(35, 'fyAMmBmyjh@gmail.com', 'kHtUJbATcc', 'pYxGyEXlRJ', 144, '', 0, NULL, NULL, 0),
	(36, 'NPwaOvqnCg@gmail.com', 'bSUFccdNUk', 'vcONdlYAWZ', 149, '', 0, NULL, NULL, 0),
	(37, 'hsgMMLWAxY@gmail.com', 'PSckedxoLk', 'fPCXNXaOsW', 151, '', 0, NULL, NULL, 0),
	(38, 'YEMAiljUOO@gmail.com', 'VKopHSUwKG', 'vKFlHvwEmJ', 152, '', 0, NULL, NULL, 0),
	(39, 'mjOvHbncFm@gmail.com', 'uaOVZBdiKa', 'SlCXISGMwF', 153, '', 0, NULL, NULL, 0),
	(40, 'ArjsKQxvvh@gmail.com', 'jPANZmeKCJ', 'dirJQaDNok', 154, 'EATtbNczhYDRdbWIChdJiZhCJklPCFhNsgmASVkSRKgppqcJKUGICwYgOBGJZingwRmgNfpsWQXJNkQZXpXOerLiXREHNRLiEwIZrjTvJPcFmrWxSjXvuQuAAPTFdDnrfcQqwsJlVHqGCEzNwkTTFSecGgyZhtQElTWYeySKgBSlYAiszuiFgrioGxDTnoIxZergzpQjNyDQxGeQsgpHxSvmnMTWOKAECQxHlwfStKeBDPWoXcbYJAnkpRrkIZbvQAdQDWeTWNotQurITysnciwqpwbxZLHUpgSQlKBKJedxXEOxTQLevlzvhhXcHrVmOSjIWFtlNikJEOczMucKSXRdOmrkpOZZQspKuapfqnzYVVVBcoQBMONPIVTeDWKwFYOWeiKhWqSMNPIZabfGihxGGZKGYgRKWcYkESGMbweAUIRYIqkhthtkXzhHvGCPgGSuOVjpgrxdtDOQysKhDbiWcQTVOfWyKILMpoFneMcjZNfEWFfC', 0, NULL, NULL, 0),
	(41, 'marto221@gmail.com', 'Martin', 'Vlachkov', 155, 'I like testing!', 350, 2, 4, 0),
	(42, 'LGiNV@gmail.com', 'Martin', 'Vlachkov', 158, 'I like testing!', 350, 2, 4, 0),
	(43, 'test1234@gmail.com', 'User10', 'Regular', 160, '', 0, NULL, NULL, 0),
	(44, 'renbSpQMCV@gmail.com', 'bAyxjgwaaD', 'ocvBGvLRJj', 161, '', 0, NULL, NULL, 0),
	(45, 'sUFFkfpNIB@gmail.com', 'mZeebiDdWX', 'UGDNTQliJL', 162, '', 0, NULL, NULL, 0),
	(46, 'CZtxipdhBC@gmail.com', 'HljGgQlZMf', 'EMsIhOnjGh', 163, 'sUFmuyQeMAyJcIkSxrVkkkJJCaiaFyxUQbbIihuBcBVITbfnmuUXqKXvUacdComLtfQbNjtgHkrcTamijJcyhenGCiULpoPEKUMOGriRxziPsCluNwexbccowObYwUlNfZRDBsENRggSdcJUGVvUuxtyMpVxrDPwpkyugrankcGFaEDVjIZkhJOeDVjPwBJWkOcMxIydGhhnOXusiGkybIIxstEzjWKvdWnkQqRRJzcUIjYXnZSgQjEvYcUyWXkOwHyDQxAqcpJaKwFosZwavacJATPYMzxYsZJqosszFizdUUKYRkgCTLAKbpVkOfwPDldsryBuKjfpHjMvanUDekIZCftXlkcpOgXQeffKhBuPtflwsMfAlgImXifXEmQJgqXyZdcsqkatCDuTpXXhoswbylWyNkbAvGdmRajAQyGzUmFGMylFQUPNQiOWQsxBbDAaOZbgzNUJSmAOVhDnguImBxuZrdVDNDTEWJYCjLTogBZeixCN', 0, NULL, NULL, 0),
	(47, 'AbXFJMJDVE@gmail.com', 'mTgvtqLyVp', 'AcqhxXVomF', 164, 'VBmSbPEKjjNEYYaCldBSGDnCBDzSyzAHnNxjdfSAjZqBbcleAhHFMNFxwNMGBwJUullofBpRDSzHQjIELwHoLpNKAsussezihtCBxtLloJzLALtKcAhGsvhGkjGsWktHnbDABnuzZjAKigVNMuTWrqfxZDtYzHPIDfDlJFWnWXGinnNUqRBcLZRdWdbDQHtCnbQjTkdYimxVArfLOZwEWMWaQesEafnFkQZvCgiGNYMSKOYjWRGxAkcLLvbcKWfrUOvSfkcTyOBKqLzeiwQjgMHGgXtxAorvcHRDFSYBBegQpjuRvCIfHKfSUzqoBTuviKyIceiErQGQCVjjbIFFNieMZzVmnRZMJzwjiaMyHOLJYEYJjuwwslsRgKHoRHHKJnDVuWFynlrQrzyYcOiHiyRgwNIFmIoRVBvmDCclottwbqxHhCQlHFFUiqFZBrNSbpTdLsllfRNWFFAOPgbBuqzFgqkVZloBhDVecMRIhHFaHhtLgQux', 0, NULL, NULL, 0),
	(48, 'EASHi@gmail.com', 'Martin', 'Vlachkov', 165, 'I like testing!', 350, 2, 4, 0),
	(49, 'ZeXet@gmail.com', 'Martin', 'Vlachkov', 166, 'I like testing!', 350, 2, 4, 0),
	(50, 'hFPwn@gmail.com', 'Martin', 'Vlachkov', 167, 'I like testing!', 350, 2, 4, 0),
	(51, 'ztSDA@gmail.com', 'Martin', 'Vlachkov', 168, 'I like testing!', 35, 2, 4, 0),
	(52, 'kZqYx@gmail.com', 'Martin', 'Vlachkov', 170, 'I like testing!', 35, 2, 4, 0),
	(53, 'oeweL@gmail.com', 'Martin', 'Vlachkov', 171, 'I like testing!', 35, 2, 4, 0),
	(54, 'RQwxu@gmail.com', 'Martin', 'Vlachkov', 172, 'I like testing!', 350, 2, 4, 0),
	(55, 'LEfTZ@gmail.com', 'Martin', 'Vlachkov', 173, 'I like testing!', 350, 2, 4, 0),
	(56, 'nluVw@gmail.com', 'Martin', 'Vlachkov', 174, 'I like testing!', 35, 2, 4, 0),
	(57, 'bRcPe@gmail.com', 'Martin', 'Vlachkov', 175, 'I like testing!', 35, 2, 4, 0),
	(58, 'BvXtj@gmail.com', 'Martin', 'Vlachkov', 176, 'I like testing!', 35, 2, 4, 0),
	(59, 'JXYcQ@gmail.com', 'Martin', 'Vlachkov', 177, 'I like testing!', 35, 2, 4, 0),
	(60, 'bpQXz@gmail.com', 'Martin', 'Vlachkov', 178, 'I like testing!', 35, 2, 4, 0),
	(61, 'nAKPV@gmail.com', 'Martin', 'Vlachkov', 179, 'I like testing!', 35, 2, 4, 0),
	(62, 'xaKyM@gmail.com', 'Martin', 'Vlachkov', 180, 'I like testing!', 35, 2, 4, 0),
	(63, 'LUCVS@gmail.com', 'Martin', 'Vlachkov', 182, 'I like testing!', 35, 2, 4, 0),
	(64, 'DlOYi@gmail.com', 'Martin', 'Vlachkov', 183, 'I like testing!', 35, 2, 4, 0),
	(65, 'LopPL@gmail.com', 'Martin', 'Vlachkov', 184, 'I like testing!', 35, 2, 4, 0),
	(66, 'NWpDv@gmail.com', 'Martin', 'Vlachkov', 185, 'I like testing!', 35, 2, 4, 0),
	(67, 'WuLgk@gmail.com', 'Martin', 'Vlachkov', 186, 'I like testing!', 35, 2, 4, 0),
	(68, 'KNtYd@gmail.com', 'Martin', 'Vlachkov', 187, 'I like testing!', 35, 2, 4, 0),
	(69, 'egcaF@gmail.com', 'Martin', 'Vlachkov', 188, 'I like testing!', 35, 2, 4, 0),
	(70, 'qTkUK@gmail.com', 'Martin', 'Vlachkov', 189, 'I like testing!', 35, 2, 4, 0),
	(71, 'deleted_user@gmail.com', 'User6', 'Regular', 205, '', 0, NULL, NULL, 0),
	(72, 'tpOXs@gmail.com', 'Martin', 'Vlachkov', 226, 'I like testing!', 35, 2, 4, 0),
	(73, 'UbyKN@gmail.com', 'Martin', 'Vlachkov', 233, 'I like testing!', 35, 2, 4, 0),
	(74, 'LQAkW@gmail.com', 'Martin', 'Vlachkov', 234, 'I like testing!', 35, 2, 4, 0),
	(75, 'KHCXP@gmail.com', 'Martin', 'Vlachkov', 236, 'I like testing!', 35, 2, 4, 0);
/*!40000 ALTER TABLE `users_details` ENABLE KEYS */;

-- Dumping structure for table narnia232_project.visibilities
CREATE TABLE IF NOT EXISTS `visibilities` (
  `visibility_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`visibility_id`),
  UNIQUE KEY `visabilities_type_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table narnia232_project.visibilities: ~2 rows (approximately)
DELETE FROM `visibilities`;
/*!40000 ALTER TABLE `visibilities` DISABLE KEYS */;
INSERT INTO `visibilities` (`visibility_id`, `type`, `enabled`) VALUES
	(1, 'public', 1),
	(2, 'connections', 1);
/*!40000 ALTER TABLE `visibilities` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
