Final Project Team Narnia

1.  Trello board -
    https://trello.com/invite/b/Hb3E2x7X/b4c39fc68b41a1f509290c54725b1b29/final-project

2.  Test Plan -
    https://telerikacademy-my.sharepoint.com/:w:/p/martin_vlachkov_a22_learn/EXnSfVvLiMFCq_9KCUtLlzABjo0hTACTxG4UHomjbt-L9g
3.  Exploratory test report -
    https://telerikacademy-my.sharepoint.com/:w:/p/martin_vlachkov_a22_learn/EXS2SYGcsuBAmUIaqRWTEtgBefQTnsYUOijk6F29748gLg

4.  Test Case Template Excel -
    https://telerikacademy-my.sharepoint.com/:x:/p/martin_vlachkov_a22_learn/EQZumk1cgzNCtl92ZKM23WkBTXyKtiicz2z6JQ7oIyT-ng

5.  Test Case TestRail -
    https://teamnarnia.testrail.com/index.php?/runs/overview/1

6.  Issues Report -
    https://telerikacademy-my.sharepoint.com/:w:/p/martin_vlachkov_a22_learn/EeqkootrprtHph8sP11x0DQBqydebYS-g28Th0m6qQ4WOw

7.  Test Report -
    https://telerikacademy-my.sharepoint.com/:w:/p/martin_vlachkov_a22_learn/EXPssD4YMYlLmr3HY-8vQDMBO-jUGcgY6T20Ex5QMzacDw


